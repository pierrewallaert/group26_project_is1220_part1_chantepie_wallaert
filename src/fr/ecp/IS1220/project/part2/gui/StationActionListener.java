package fr.ecp.IS1220.project.part2.gui;

import java.awt.FlowLayout;
import javax.swing.*;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationFactory;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class StationActionListener extends JFrame implements ActionListener {
	GraphicInterface gI;
	JLabel velibLabel = new JLabel("Velib System");
	JComboBox<String> velibBox;
	JLabel typeLabel = new JLabel("Type");
	String [] type = {"Standard", "Plus"};
	JComboBox<String> typeBox = new JComboBox<String>(type);
	JLabel numLabel = new JLabel("Number of Slots");
	JTextField numSlots = new JTextField(10);
	JLabel labelLatitude = new JLabel("Latitude");
	JTextField latitudeField = new JTextField(10);
	JLabel labelLongitude = new JLabel("Longitude");
	JTextField longitudeField = new JTextField(10);
	JButton b = new JButton("Ok"); // a button
	

	
	public StationActionListener(String title, GraphicInterface gI) {
		super(title);
		this.gI = gI;
		setLayout(new FlowLayout());
		add(velibLabel);
		String [] velibList = new String[10];
		int i=0;
		for (VelibSystem vS : gI.velibList) {
			velibList[i] = vS.getName();
			i++;
		}
		velibBox = new JComboBox<String>(velibList);
		add(velibBox);
		add(typeLabel);
		add(typeBox);
		add(numLabel);
		add(numSlots);
		add(labelLatitude);
		add(latitudeField);
		add(labelLongitude);
		add(longitudeField);
		add(b); // add the button to the main window
		b.addActionListener(this); // listen to the added button
		this.pack();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.setVisible(true);
		String velibName = velibBox.getSelectedItem().toString();
		String stationType = typeBox.getSelectedItem().toString();
		int numberSlots = Integer.parseInt(numSlots.getText());
		double latitude = Double.parseDouble(latitudeField.getText());
		double longitude = Double.parseDouble(longitudeField.getText());
		VelibSystem myVelib = new VelibSystem("null");
		Boolean ok = false;
		for (VelibSystem vS : gI.getVelibList()) {
			if (vS.getName()==velibName) {
				myVelib = vS;
				ok = true;
			}
		}
		if (ok == true) {
			if (stationType == "Standard") {
				Station station = StationFactory.createStation(new GpsCoordinates(latitude, longitude), StationType.Standard, numberSlots, myVelib);
				JFrame display = new JFrame(); 
				JTextArea areaStation = new JTextArea(station.toString()); 
				display.add(areaStation); 
				display.setVisible(true);
				display.pack();
				display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
			else if (stationType == "Plus") {
				Station station = StationFactory.createStation(new GpsCoordinates(latitude, longitude), StationType.Plus, numberSlots, myVelib);
				JFrame display = new JFrame(); 
				JTextArea areaStation = new JTextArea(station.toString()); 
				display.add(areaStation); 
				display.setVisible(true);
				display.pack();
				display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
		}
	}

}
