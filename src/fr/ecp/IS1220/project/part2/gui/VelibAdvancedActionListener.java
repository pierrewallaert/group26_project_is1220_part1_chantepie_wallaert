package fr.ecp.IS1220.project.part2.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;

public class VelibAdvancedActionListener extends JFrame implements ActionListener {
	JLabel nameLabel = new JLabel("Name");
	JTextField nameText = new JTextField(50);
	JLabel nStationLabel = new JLabel("Number of stations");
	JTextField nStationText = new JTextField(5);
	JLabel nSlotLabel = new JLabel("Number of slots");
	JTextField nSlotText = new JTextField(5);
	JLabel sideLabel = new JLabel("Side area");
	JTextField sideText = new JTextField(10);
	JLabel bikeLabel = new JLabel("Number of bicycles");
	JTextField bikeText = new JTextField(5);
	JButton b = new JButton("Ok"); // a button
	GraphicInterface gI;
	
	public VelibAdvancedActionListener(String title, GraphicInterface gI) {
		super(title);
		this.gI = gI;
		setLayout(new FlowLayout());
		add(nameLabel);
		add(nameText); 
		add(nStationLabel);
		add(nStationText);
		add(nSlotLabel);
		add(nSlotText);
		add(sideLabel);
		add(sideText);
		add(bikeLabel);
		add(bikeText);
		add(b); // add the button to the main window
		b.addActionListener(this); // listen to the added button
		this.pack();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.setVisible(true);
		
		VelibSystem myVelib = new VelibSystem(nameText.getText(),Integer.parseInt(nStationText.getText()),Integer.parseInt(nSlotText.getText()),Integer.parseInt(sideText.getText()),Integer.parseInt(bikeText.getText()));
		JFrame display = new JFrame();
		JTextArea areaVelib = new JTextArea("Created"); 
		display.add(areaVelib); 
		display.setVisible(true);
		display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

}
