package fr.ecp.IS1220.project.part2.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class UserActionListener extends JFrame implements ActionListener {
	JLabel velibLabel = new JLabel("Velib System");
	JComboBox<String> velibBox;
	JTextField surnameField = new JTextField(40); // an area to display text up to 40 characters
	JTextField nameField = new JTextField(40);
	JTextField latitudeField = new JTextField(10);
	JTextField longitudeField = new JTextField(10);
	JButton b = new JButton("Ok"); // a button
	JLabel labelSurname = new JLabel("Surname");
	JLabel labelName = new JLabel("Name");
	JLabel labelLatitude = new JLabel("Latitude");
	JLabel labelLongitude = new JLabel("Longitude");
	JLabel labelType = new JLabel("Type");
	String [] type = {"Guest", "Vlibre", "VMax"};
	JComboBox<String> userType = new JComboBox<String>(type);
	GraphicInterface gI;
	
	
	public UserActionListener(String title, GraphicInterface gI) {
		super(title);
		this.gI = gI;
		setLayout(new FlowLayout());
		add(velibLabel);
		String [] velibList = new String[10];
		int i=0;
		for (VelibSystem vS : gI.velibList) {
			velibList[i] = vS.getName();
			i++;
		}
		velibBox = new JComboBox<String>(velibList);
		add(velibBox);
		add(labelSurname);
		add(surnameField); // add the text area to the main window
		add(labelName);
		add(nameField);
		add(labelLatitude);
		add(latitudeField);
		add(labelLongitude);
		add(longitudeField);
		add(labelType);
		add(userType);
		add(b); // add the button to the main window
		b.addActionListener(this); // listen to the added button
		this.pack();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		this.setVisible(true);
		String surname = surnameField.getText();
		String name = nameField.getText();
		double latitude = Double.parseDouble(latitudeField.getText());
		double longitude = Double.parseDouble(longitudeField.getText());
		if (userType.getSelectedItem().toString() == "Guest") {
			User user = UserFactory.createUser(name, surname, UserType.Guest, new GpsCoordinates(latitude, longitude));
			this.gI.getUserList().add(user);
			JFrame display = new JFrame(); 
			JTextArea areaUser = new JTextArea(user.toString()); 
			display.add(areaUser); 
			display.setVisible(true);
			display.pack();
			display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			 
		}
		else if (userType.getSelectedItem().toString() == "VLibre") {
			User user = RegisteredUserFactory.createRegisteredUser(name, surname, new GpsCoordinates(latitude, longitude), CardType.VLibre);
			this.gI.getUserList().add(user);
			JFrame display = new JFrame();
			JTextArea areaUser = new JTextArea(user.toString()); 
			display.add(areaUser); 
			display.setVisible(true);
			display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			 
		}
		else if (userType.getSelectedItem().toString() == "VMax") {
			User user = RegisteredUserFactory.createRegisteredUser(name, surname, new GpsCoordinates(latitude, longitude), CardType.VMax);
			this.gI.getUserList().add(user);
			JFrame display = new JFrame();
			JTextArea areaUser = new JTextArea(user.toString()); 
			display.add(areaUser); 
			display.setVisible(true);
			display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}
	}
	
	
	
}