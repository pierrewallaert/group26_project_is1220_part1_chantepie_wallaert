package fr.ecp.IS1220.project.part2.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;

public class VelibActionListener extends JFrame implements ActionListener {
	GraphicInterface gI;
	JLabel nameLabel = new JLabel("Name");
	JTextField nameText = new JTextField(50);
	JButton b = new JButton("Ok"); // a button
	
	public VelibActionListener(String title, GraphicInterface gI) {
		super(title);
		this.gI = gI;
		setLayout(new FlowLayout());
		add(nameLabel);
		add(nameText); 
		add(b); // add the button to the main window
		b.addActionListener(this); // listen to the added button
		this.pack();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.setVisible(true);
		
		VelibSystem myVelib = new VelibSystem(nameText.getText());
		JFrame display = new JFrame();
		JTextArea areaVelib = new JTextArea("Created"); 
		display.add(areaVelib); 
		display.setVisible(true);
		display.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


	}

}
