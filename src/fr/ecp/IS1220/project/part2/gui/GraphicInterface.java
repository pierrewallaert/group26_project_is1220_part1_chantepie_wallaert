package fr.ecp.IS1220.project.part2.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.*;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.users.User;



public class GraphicInterface {
	ArrayList<VelibSystem> velibList = new ArrayList<VelibSystem>();
	ArrayList<User> userList = new ArrayList<User>();
	
	
	public static void main(String[] args) {
	VelibSystem myVelib = new VelibSystem("Yoyo");
	GraphicInterface gI = new GraphicInterface();
	gI.addVelib(myVelib);
	// Create a JFrame (with an optional title)
	JFrame frame = new JFrame("Velib System");
	// link the closure of the window to application exit
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	// adapt automatically its size to its content:
	frame.pack();

	
	
	//JPanel Creation
	JTabbedPane tabbedPane = new JTabbedPane();
	JPanel panel1 = new JPanel(new FlowLayout());
	tabbedPane.addTab("Creation", panel1);
	JButton userCreate = new JButton("User");
	userCreate.addActionListener(new UserActionListener("User Creation", gI));
	userCreate.setVisible(true);
	JButton stationCreate = new JButton("Station");
	stationCreate.addActionListener(new StationActionListener("Station Creation", gI));
	stationCreate.setVisible(true);
	JButton velibCreate = new JButton("Velib System");
	velibCreate.addActionListener(new VelibConfiguration("Velib Creation", gI));
	velibCreate.setVisible(true);;
	panel1.add(velibCreate);
	panel1.add(stationCreate);
	panel1.add(userCreate);
	frame.setContentPane(tabbedPane);
	
	//JPanel Gestion
	JPanel panel2 = new JPanel(new FlowLayout());
	tabbedPane.addTab("Gestion", panel2);
	panel2.setVisible(true);
	JPanel subpanel1 = new JPanel(new FlowLayout());
	
	frame.setContentPane(tabbedPane);
	
	//Set visible
	frame.setVisible(true);
	frame.getContentPane().setVisible(true);
	}
	
	

	
	
	
	
	
	
	
	
	public ArrayList<VelibSystem> getVelibList() {
		return velibList;
	}


	public void setVelibList(ArrayList<VelibSystem> velibList) {
		this.velibList = velibList;
	}
	
	public void addVelib(VelibSystem velib) {
		this.velibList.add(velib);
	}

	public void addVelib(ArrayList<VelibSystem> velibList) {
		for (VelibSystem vS : velibList) {
			this.velibList.add(vS);
		}

	}

	public ArrayList<User> getUserList() {
		return userList;
	}


	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}
	
	public void addUser(User user) {
		this.userList.add(user);
	}

	public void addUser(ArrayList<User> userList) {
		for (User us : userList) {
			this.userList.add(us);
		}

	}
}
