package fr.ecp.IS1220.project.part2.gui;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class VelibConfiguration extends JFrame implements ActionListener {
	JButton b1 = new JButton("Basique");
	JButton b2 = new JButton("Avanc�e");
	GraphicInterface gI;
	
	
	
	
	public VelibConfiguration(String title, GraphicInterface gI) throws HeadlessException {
		super(title);
		this.setLayout(new FlowLayout());
		this.gI = gI;
		add(b1);
		b1.addActionListener(new VelibActionListener("Base", gI));
		add(b2);
		b2.addActionListener(new VelibAdvancedActionListener("Advanced", gI));
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		this.setVisible(true);
	}

}
