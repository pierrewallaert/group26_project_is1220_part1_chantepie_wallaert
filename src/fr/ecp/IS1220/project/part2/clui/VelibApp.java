package fr.ecp.IS1220.project.part2.clui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.omg.CORBA.SystemException;

import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.ParkingSlots;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.Guest;
import fr.ecp.IS1220.project.part1.users.LibreClient;
import fr.ecp.IS1220.project.part1.users.MaxClient;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Moment;

public class VelibApp {
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_RESET = "\u001B[0m";
	Map<String, VelibSystem> velibSystems ;
	
	
	
	public VelibApp() {
		super();
		this.velibSystems= new HashMap<>();
	}


	public void execute(String[] linesplit) {
		if (linesplit[0].equals("setup")) {
			if (!velibSystems.containsKey(linesplit[1])) {
				if (linesplit.length == 2) {
					velibSystems.put(linesplit[1], new VelibSystem(linesplit[1]));
				} else if (linesplit.length == 6) {
					velibSystems.put(linesplit[1],
							new VelibSystem(linesplit[1], (int) Integer.parseInt(linesplit[2]),
									(int) Integer.parseInt(linesplit[3]), (int) Integer.parseInt(linesplit[4]),
									(int) Integer.parseInt(linesplit[5])));
				} 
			}
		}
		else if (linesplit[0].equals("offline")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				System.out.println();
				my_velib.getStationFromID((int)Integer.parseInt(linesplit[2])).setStationStatus(StationStatus.Offline);;
			} catch (NumberFormatException e) {
				System.err.println("Invalid Station ID");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
		}
		else if (linesplit[0].equals("online")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				my_velib.getStationFromID((int)Integer.parseInt(linesplit[2])).setStationStatus(StationStatus.OnService);;
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.err.println("Invalid Station ID");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
		}
		else if (linesplit[0].equals("addUser")) {
			try {
				String userName=linesplit[2];
				String cardType=linesplit[3];
				
				VelibSystem my_velib= velibSystems.get(linesplit[1]);

				if (cardType.equals("none")){
					Guest guest=new Guest(userName,"",new GpsCoordinates(0,0));
					my_velib.addUser(guest);
					System.out.println("Your User ID is:"+guest.getID());
				}
				else {
					if (cardType.toLowerCase().equals("libre")){
						LibreClient user=new LibreClient(userName,"",new GpsCoordinates(0,0));
						my_velib.addUser(user);
						System.out.println("Your User ID is:"+user.getID());
					}
					if(cardType.toLowerCase().equals("max")) {
						MaxClient user=new MaxClient(userName,"",new GpsCoordinates(0,0));
						my_velib.addUser(user);
						System.out.println("Your User ID is:"+user.getID());
					}
					else {
						System.err.println("unvalid userCard type: Choose none, libre or max");
					}
				}
			}
			
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			
		}
		else if(linesplit[0].equals("rentBike")){
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				int userID=(int)Integer.parseInt(linesplit[2]);
				int stationID=(int)Integer.parseInt(linesplit[3]);
				User user=my_velib.getUserFromID(userID);
				Station station=my_velib.getStationFromID(stationID);
				boolean rent=false;
				if(station.getStationStatus()==StationStatus.Offline) {
					System.err.println("Station offline");
					
					
				}
				else {
					if (linesplit.length==4) {
						for(ParkingSlots slot: station.getSlotList()){
							if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available) {
								user.rent(slot);
								rent=true;
								System.out.println("bike rented");
								
								break;
								
							}
						}
						
						if (rent==false) {
							System.err.println("No bike available");
							
						}
					}
					else if (linesplit.length==5) {
						if (linesplit[5].equals("elec")){
							for(ParkingSlots slot: station.getSlotList()){
								if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available && slot.getCurrentBike().getType()==BicycleType.Electrical) {
									user.rent(slot);
									rent=true;
									System.out.println("bike rented");
									
									break;
									
								}
						}
							
					}
						else if (linesplit[5].equals("mech")){
							for(ParkingSlots slot: station.getSlotList()){
								if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available && slot.getCurrentBike().getType()==BicycleType.Mechanical) {
									user.rent(slot);
									rent=true;
									System.out.println("bike rented");
									
									break;
									
								}
						}
						
							
					}
						else {
							System.err.println("Please enter a valid bike type");
							
						}
					}
				}
				

				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.err.println("Invalid Station ID");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			
		}
		else if(linesplit[0].equals("returnBike")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				Station station=my_velib.getStationFromID((int)Integer.parseInt(linesplit[3]));
				User user=my_velib.getUserFromID((int)Integer.parseInt(linesplit[2]));
				boolean end=false;
				if(station.getStationStatus()==StationStatus.Offline) {
					System.err.println("Station offline");
					
				}
				else if (linesplit.length==4) {
					for(ParkingSlots slot: station.getSlotList()){
						if (slot.getOccupation()==ParkingSlotOccupation.Free && slot.getStatus()==ParkingSlotStatus.Available) {
							user.endRent(slot);
							end=true;
							break;
							
						}
					}
					if (end==false) {
						System.err.println("No slot available");
					}
				}
				else if(linesplit.length==5) {
					for(ParkingSlots slot: station.getSlotList()){
						if (slot.getOccupation()==ParkingSlotOccupation.Free && slot.getStatus()==ParkingSlotStatus.Available) {
							user.endRent(slot,(int)Integer.parseInt(linesplit[3]));
							end=true;
							break;
							
						}
					}
					if (end==false) {
						System.err.println("No slot available");
					}
				}
				else {
					System.err.println("Invalid number of arguments");
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			
		}
		else if (linesplit[0].equals("displayStation")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				Station station=my_velib.getStationFromID((int)Integer.parseInt(linesplit[2]));
				station.showStats();
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("displayUser")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				User user=my_velib.getUserFromID((int)Integer.parseInt(linesplit[2]));
				user.showStats();
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("sortStation")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				if(linesplit[2].toLowerCase().equals("leastoccupied")) {
					System.out.println(my_velib.getLeastOccupiedStation(my_velib.creation, new Moment())+" \n");
				}
				if(linesplit[2].toLowerCase().equals("mostused")) {
					System.out.println(my_velib.getMostUsedStation());
				}
				else {
					System.err.println("Unknown sort strategy");
				}
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name ");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("runTest")) {
			ArrayList<String> returnValue= new ArrayList<String>() ;
			FileReader file = null;
			BufferedReader reader = null;
			
			File filew = new File(linesplit[1]+"Output.txt");
			// creates the file
			try {
				filew.createNewFile();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			// creates a FileWriter Object
			FileWriter writer = null;
			try {
				writer = new FileWriter(filew);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} // Writes the content to the file writer.write(”This\n is\n an\n example\n”); writer.flush();
			
			try {
			file = new FileReader(linesplit[1]); // a FileReader for reading byte−by−byte
			reader = new BufferedReader(file); // wrapping a FileReader into a BufferedReader for reading line−by−
			
			String line = "";
			try {
				while ((line = reader.readLine()) != null) { // read the file line−by−line
					returnValue.add(line);
				
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			catch (Exception e) {
			System.err.println("File not existing");
			} finally {
			if (reader != null) {
				try {reader.close();}
			catch (IOException e) {// Ignore issues 
				}
			}
			if (file != null) {
			try {file.close();}
			catch (IOException f) { // Ignore issues during closing 
				}
			}
			}
			
			for(String line: returnValue) {
				String[] linesplited= line.split(" ");
				try {
					this.executeWrite(linesplited, writer);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Unknown command");
		}
		System.out.print(">>");
	}
			
	public void executeWrite(String[] linesplit,FileWriter writer) throws IOException {
		if (linesplit[0].equals("setup")) {
			if (!velibSystems.containsKey(linesplit[1])) {
				if (linesplit.length == 2) {
					velibSystems.put(linesplit[1], new VelibSystem(linesplit[1]));
				} else if (linesplit.length == 6) {
					velibSystems.put(linesplit[1],
							new VelibSystem(linesplit[1], (int) Integer.parseInt(linesplit[2]),
									(int) Integer.parseInt(linesplit[3]), (int) Integer.parseInt(linesplit[4]),
									(int) Integer.parseInt(linesplit[5])));
				} 
			}
		}
		else if (linesplit[0].equals("offline")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				
				my_velib.getStationFromID((int)Integer.parseInt(linesplit[2])).setStationStatus(StationStatus.Offline);;
				System.out.println("Station "+linesplit[2]+" offline ");
				writer.write("Station "+linesplit[2]+" offline \n");
			} catch (NumberFormatException e) {
				System.err.println("Invalid Station ID");
				writer.write("Invalid Station ID \n");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist ");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments \n");
			}
		}
		else if (linesplit[0].equals("online")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				my_velib.getStationFromID((int)Integer.parseInt(linesplit[2])).setStationStatus(StationStatus.OnService);;
				System.out.println("Station "+linesplit[2]+" online ");
				writer.write("Station "+linesplit[2]+" online \n");
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.err.println("Invalid Station ID");
				writer.write("Invalid Station ID \n");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments \n");
			}
		}
		else if (linesplit[0].equals("addUser")) {
			try {
				String userName=linesplit[2];
				String cardType=linesplit[3];
				
				VelibSystem my_velib= velibSystems.get(linesplit[1]);

				if (cardType.equals("none")){
					Guest guest=new Guest(userName,"",new GpsCoordinates(0,0));
					my_velib.addUser(guest);
					System.out.println("Your User ID is:"+guest.getID());
					writer.write("Your User ID is:"+guest.getID()+ "\n");
				}
				else {
					if (cardType.toLowerCase().equals("libre")){
						LibreClient user=new LibreClient(userName,"",new GpsCoordinates(0,0));
						my_velib.addUser(user);
						System.out.println("Your User ID is:"+user.getID());
						writer.write("Your User ID is:"+user.getID()+ "\n");
					}
					else if(cardType.toLowerCase().equals("max")) {
						MaxClient user=new MaxClient(userName,"",new GpsCoordinates(0,0));
						my_velib.addUser(user);
						System.out.println("Your User ID is:"+user.getID());
						writer.write("Your User ID is:"+user.getID()+ "\n");
					}
					else {
						System.err.println("unvalid userCard type: Choose none, libre or max");
						writer.write("unvalid userCard type: Choose none, libre or max \n");
					}
				}
			}
			
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments \n");
			}
			
		}
		else if(linesplit[0].equals("rentBike")){
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				int userID=(int)Integer.parseInt(linesplit[2]);
				int stationID=(int)Integer.parseInt(linesplit[3]);
				User user=my_velib.getUserFromID(userID);
				Station station=my_velib.getStationFromID(stationID);
				boolean rent=false;
				if(station.getStationStatus()==StationStatus.Offline) {
					System.err.println("Station offline");
					writer.write("Station offline \n");
					
				}
				else {
					if (linesplit.length==4) {
						for(ParkingSlots slot: station.getSlotList()){
							if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available) {
								user.rent(slot);
								rent=true;
								System.out.println("bike rented");
								writer.write("bike rented \n ");
								break;
								
							}
						}
						
						if (rent==false) {
							System.err.println("No bike available");
							writer.write("No bike available \n");
						}
					}
					else if (linesplit.length==5) {
						if (linesplit[4].equals("elec")){
							for(ParkingSlots slot: station.getSlotList()){
								if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available && slot.getCurrentBike().getType()==BicycleType.Electrical) {
									user.rent(slot);
									rent=true;
									System.out.println("bike rented");
									writer.write("bike rented \n ");
									break;
									
								}
						}
							
					}
						else if (linesplit[4].equals("mech")){
							for(ParkingSlots slot: station.getSlotList()){
								if (slot.getOccupation()==ParkingSlotOccupation.Occupied && slot.getStatus()==ParkingSlotStatus.Available && slot.getCurrentBike().getType()==BicycleType.Mechanical) {
									user.rent(slot);
									rent=true;
									System.out.println("bike rented");
									writer.write("bike rented \n ");
									break;
									
								}
						}
						
							
					}
						else {
							System.err.println("Please enter a valid bike type");
							writer.write("non valid bike type \n");
						}
					}
				}

				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.err.println("Invalid Station ID");
				writer.write("Invalid Station ID \n");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments \n");
			}
			
		}
		else if(linesplit[0].equals("returnBike")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				Station station=my_velib.getStationFromID((int)Integer.parseInt(linesplit[3]));
				User user=my_velib.getUserFromID((int)Integer.parseInt(linesplit[2]));
				boolean end=false;
				if(station.getStationStatus()==StationStatus.Offline) {
					System.err.println("Station offline");
					writer.write("Station offline \n");
					
				}
				else if (linesplit.length==4) {
					for(ParkingSlots slot: station.getSlotList()){
						if (slot.getOccupation()==ParkingSlotOccupation.Free && slot.getStatus()==ParkingSlotStatus.Available) {
							user.endRent(slot);
							end=true;
							break;
							
						}
					}
					if (end==true) {
						writer.write(user.getName()+" has paid "+user.getRides().get(user.getRideNumber()-1).cost.cost().get(0)+" \n");
					}
					if (end==false) {
						System.err.println("No slot available");
						writer.write("No slot available \n");
					}
				}
				else if(linesplit.length==5) {
					for(ParkingSlots slot: station.getSlotList()){
						if (slot.getOccupation()==ParkingSlotOccupation.Free && slot.getStatus()==ParkingSlotStatus.Available) {
							user.endRent(slot,(int)Integer.parseInt(linesplit[4]));
							end=true;
							break;
							
						}
					}
					if (end==false) {
						System.err.println("No slot available");
						writer.write("No slot available \n");
					}
				}
				else {
					System.err.println("Invalid  arguments");
					writer.write("Invalid arguments \n");
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments \n");
			}
		}
		else if (linesplit[0].equals("displayStation")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				Station station=my_velib.getStationFromID((int)Integer.parseInt(linesplit[2]));
				System.out.println(station.showStats());
				writer.write(station.showStats()+"\n");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist \n");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments ");
				writer.write("invalid number of arguments \n");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
				writer.write("invalid user number format \n");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("displayUser")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				User user=my_velib.getUserFromID((int)Integer.parseInt(linesplit[2]));
				System.out.println(user.showStats());
				writer.write(user.showStats()+"\n");
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name or this station ID doesn't exist");
				writer.write("invalid velib name or this station ID doesn't exist");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
				writer.write("invalid user number format");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("sortStation")) {
			try {
				VelibSystem my_velib= velibSystems.get(linesplit[1]);
				if(linesplit[2].toLowerCase().equals("leastoccupied")) {
					System.out.println(my_velib.getLeastOccupiedStation(my_velib.creation, new Moment()));
					writer.write(my_velib.getLeastOccupiedStation(my_velib.creation, new Moment()).toString()+"\n");
				}
				else if(linesplit[2].toLowerCase().equals("mostused")) {
					System.out.println(my_velib.getMostUsedStation());
					writer.write(my_velib.getMostUsedStation().toString());
				}
				else {
					System.err.println("Unknown sort strategy");
					writer.write("Unknown sort strategy");
				}
			}
			catch(NullPointerException e) {
				System.err.println("invalid velib name ");
				writer.write("invalid velib name ");
			}
			catch(IndexOutOfBoundsException f) {
				System.err.println("invalid number of arguments");
				writer.write("invalid number of arguments");
			}
			catch(NumberFormatException g) {
				System.err.println("invalid user number format");
				writer.write("invalid user number format");
			}
			finally {}
			
		}
		else if (linesplit[0].equals("runTest")) {
			ArrayList<String> returnValue= new ArrayList<String>() ;
			FileReader file = null;
			BufferedReader reader = null;
			
			File filew = new File(linesplit[1]+"Output.txt");
			// creates the file
			try {
				filew.createNewFile();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			// creates a FileWriter Object
			FileWriter writer2 = null;
			try {
				writer = new FileWriter(filew);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} // Writes the content to the file writer.write(”This\n is\n an\n example\n”); writer.flush();
			
			try {
			file = new FileReader(linesplit[1]); // a FileReader for reading byte−by−byte
			reader = new BufferedReader(file); // wrapping a FileReader into a BufferedReader for reading line−by−
			
			String line = "";
			try {
				while ((line = reader.readLine()) != null) { // read the file line−by−line
					returnValue.add(line);
				
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			catch (Exception e) {
			System.err.println("File not existing");
			writer.write("File not existing");
			} finally {
			if (reader != null) {
				try {reader.close();}
			catch (IOException e) {// Ignore issues 
				}
			}
			if (file != null) {
			try {file.close();}
			catch (IOException f) { // Ignore issues during closing 
				}
			}
			}
			for(String line: returnValue) {
				String[] linesplited= line.split(" ");
				try {
					this.executeWrite(linesplited, writer2);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Unknown command");
			writer.write("Unknown command");
		}
		System.out.print(">>");
	}
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		Map<String, VelibSystem> velibSystems = new HashMap<>();
		VelibApp velibApp=new VelibApp();
		String init="runTest setup.ini";
		String[] inisplit= init.split(" ");
		velibApp.execute(inisplit);
		System.out.print(">>");
		while(true) {
			VelibSystem my_velib= null;
			String line =sc.nextLine();
			
			String[] linesplit= line.split(" ");
			
			
			velibApp.execute(linesplit);
			
		
		}

	}
}
