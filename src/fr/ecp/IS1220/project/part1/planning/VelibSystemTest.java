package fr.ecp.IS1220.project.part1.planning;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

class VelibSystemTest {
	ArrayList<GpsCoordinates> stationCoord = new ArrayList<GpsCoordinates>();

	
	
	@Test
	void testStationNumber() {
		stationCoord.add(new GpsCoordinates(0,0));
		stationCoord.add(new GpsCoordinates(1,0));
		stationCoord.add(new GpsCoordinates(0,1));
		stationCoord.add(new GpsCoordinates(1,1));
		stationCoord.add(new GpsCoordinates(2,0));
		stationCoord.add(new GpsCoordinates(0,2));
		stationCoord.add(new GpsCoordinates(2,1));
		stationCoord.add(new GpsCoordinates(1,2));
		stationCoord.add(new GpsCoordinates(2,2));
		stationCoord.add(new GpsCoordinates(3,3));
		VelibSystem myVelib = new VelibSystem("myVelib", 10, 50, 0.7, 0.3, stationCoord);
		assert (myVelib.getStationsList().size()==10);
	}

	@Test
	void testSlotNumber() {
		stationCoord.add(new GpsCoordinates(0,0));
		stationCoord.add(new GpsCoordinates(1,0));
		stationCoord.add(new GpsCoordinates(0,1));
		stationCoord.add(new GpsCoordinates(1,1));
		stationCoord.add(new GpsCoordinates(2,0));
		stationCoord.add(new GpsCoordinates(0,2));
		stationCoord.add(new GpsCoordinates(2,1));
		stationCoord.add(new GpsCoordinates(1,2));
		stationCoord.add(new GpsCoordinates(2,2));
		stationCoord.add(new GpsCoordinates(3,3));
		VelibSystem myVelib = new VelibSystem("myVelib", 10, 50, 0.7, 0.3, stationCoord);
		assert (myVelib.getStationsList().get(0).getNumberOfSlots()==5);
	}
	
	@Test
	void testTotalSlotNumber() {
		stationCoord.add(new GpsCoordinates(0,0));
		stationCoord.add(new GpsCoordinates(1,0));
		stationCoord.add(new GpsCoordinates(0,1));
		stationCoord.add(new GpsCoordinates(1,1));
		stationCoord.add(new GpsCoordinates(2,0));
		stationCoord.add(new GpsCoordinates(0,2));
		stationCoord.add(new GpsCoordinates(2,1));
		stationCoord.add(new GpsCoordinates(1,2));
		stationCoord.add(new GpsCoordinates(2,2));
		stationCoord.add(new GpsCoordinates(3,3));
		VelibSystem myVelib = new VelibSystem("myVelib", 10, 50, 0.7, 0.3, stationCoord);
		int sum = 0;
		for (int i=0; i<10;i++) {
			sum += myVelib.getStationsList().get(i).getNumberOfSlots();
		}
		assert (sum==50);
	}
	
	@Test
	void testOccupation() {
		stationCoord.add(new GpsCoordinates(0,0));
		stationCoord.add(new GpsCoordinates(1,0));
		stationCoord.add(new GpsCoordinates(0,1));
		stationCoord.add(new GpsCoordinates(1,1));
		stationCoord.add(new GpsCoordinates(2,0));
		stationCoord.add(new GpsCoordinates(0,2));
		stationCoord.add(new GpsCoordinates(2,1));
		stationCoord.add(new GpsCoordinates(1,2));
		stationCoord.add(new GpsCoordinates(2,2));
		stationCoord.add(new GpsCoordinates(3,3));
		VelibSystem myVelib = new VelibSystem("myVelib", 10, 50, 0.7, 0.3, stationCoord);
		assert (myVelib.getStationsList().get(0).getNumberOfSlotsWichAre(ParkingSlotOccupation.Occupied)==Math.round(0.7*5));
	}
	
	@Test
	void testOccupationBike() {
		stationCoord.add(new GpsCoordinates(0,0));
		stationCoord.add(new GpsCoordinates(1,0));
		stationCoord.add(new GpsCoordinates(0,1));
		stationCoord.add(new GpsCoordinates(1,1));
		stationCoord.add(new GpsCoordinates(2,0));
		stationCoord.add(new GpsCoordinates(0,2));
		stationCoord.add(new GpsCoordinates(2,1));
		stationCoord.add(new GpsCoordinates(1,2));
		stationCoord.add(new GpsCoordinates(2,2));
		stationCoord.add(new GpsCoordinates(3,3));
		VelibSystem myVelib = new VelibSystem("myVelib", 10, 50, 0.7, 0.3, stationCoord);
		assert (myVelib.getStationsList().get(0).getNumberOfSlotsWichAre(BicycleType.Electrical)==Math.round(0.3*0.7*5+1));
	}
	

}
