package fr.ecp.IS1220.project.part1.planning;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Path;

public class MinimalWalkingDistance implements PlanningStrategy {

	/**
	 * @author Arnaud
	 * @param velib The VelibSystem you are currently working on 
	 * @param departure the departure coordinates of the customer
	 * @param arrival the destination coordinates
	 * @param bikeType the kind of bike the user wants to ride
	 * @return the path the customer as to follow acording to the policy he chose
	 */
	@Override
	public Path getPath(VelibSystem velib, GpsCoordinates departure, GpsCoordinates arrival, BicycleType bikeType) {
		StationsGraphe SG = new StationsGraphe(velib, departure, arrival);
		SG.setForShortestPath();
		int NMax = velib.stationsList.size();
		double [][] possiblePath = new double[NMax][NMax];
		double min = SG.getGraphe()[NMax][0]+SG.getGraphe()[1][NMax];
		int imin = 0;
		int jmin = 1;
		Boolean changed = false;
		for (int i=0; i<NMax;i++) {
			if (velib.stationsList.get(i).getStationStatus() == StationStatus.OnService) {
				Boolean bool = false;
				for (int k = 1; k < velib.stationsList.get(i).getNumberOfSlots();k++) {
					if (velib.stationsList.get(i).getSlotList().get(k).getOccupation() == ParkingSlotOccupation.Occupied &&
							velib.stationsList.get(i).getSlotList().get(k).getStatus() == ParkingSlotStatus.Available) {
						bool = true;
					}
				}
				if (bool == true) { 
					for (int j=0; j<NMax; j++) {
						if (velib.stationsList.get(j).getStationStatus() == StationStatus.OnService && i!=j) {
							Boolean bool2 = false;
							for (int k = 1; k < velib.stationsList.get(j).getNumberOfSlots();k++) {
								if (velib.stationsList.get(j).getSlotList().get(k).getOccupation() == ParkingSlotOccupation.Free &&
										velib.stationsList.get(j).getSlotList().get(k).getStatus() == ParkingSlotStatus.Available) {
									bool2 = true;
								}
							}
							if (bool2 == true) {
								double distance;
								distance = SG.getGraphe()[NMax][i]+SG.getGraphe()[j][NMax];
								possiblePath[i][j] = distance;
								if (distance<=min) {
									min = distance;
									imin=i;
									jmin=j;
									changed = true;
									
									
								}
							}	
						}
					}
				}
			}
		}
		
		if (changed == true) {
			//Uniformity Policy
			Boolean firstUniformityStart = true;
			for (int i=0;i<NMax; i++) {
				if(SG.getGraphe()[NMax][i]<=1.05*SG.getGraphe()[NMax][imin]) {
					if (velib.getStationsList().get(i).getNumberOfSlotsWichAre(bikeType) > velib.getStationsList().get(imin).getNumberOfSlotsWichAre(bikeType)) {
						if (firstUniformityStart == true) {
							min = SG.getGraphe()[NMax][i]+SG.getGraphe()[jmin][NMax];
							imin = i;
							firstUniformityStart = false;
						}
						if (firstUniformityStart == false && SG.getGraphe()[NMax][i]+SG.getGraphe()[jmin][NMax]<min) {
							min = SG.getGraphe()[NMax][i]+SG.getGraphe()[jmin][NMax];
							imin = i;
						}
					}
				}
			}
			Boolean firstUniformityEnd = true;
			for (int j=0;j<NMax; j++) {
				if(SG.getGraphe()[j][NMax]<=1.05*SG.getGraphe()[jmin][NMax] && 
					velib.getStationsList().get(j).getNumberOfSlotsWichAre(ParkingSlotOccupation.Free) > 
					velib.getStationsList().get(jmin).getNumberOfSlotsWichAre(ParkingSlotOccupation.Free)) {
					if (firstUniformityEnd == true) {
						min = SG.getGraphe()[NMax][imin]+SG.getGraphe()[j][NMax];
						jmin = j;
						firstUniformityEnd = false;
					}
					if (firstUniformityEnd == false && SG.getGraphe()[NMax][imin]+SG.getGraphe()[j][NMax]<min) {
						min = SG.getGraphe()[NMax][imin]+SG.getGraphe()[j][NMax];
						jmin = j;
					}
				}
			}
			Path path = new Path(velib.stationsList.get(imin), velib.stationsList.get(jmin));
			return path;
		}
		else {return null;}
	}
}
