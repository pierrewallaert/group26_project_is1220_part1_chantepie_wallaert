package fr.ecp.IS1220.project.part1.planning;

import java.util.ArrayList;
import java.util.Arrays;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.bicycles.MechanicalBicycle;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class StationsGraphe {
	VelibSystem myVelib;
	ArrayList<Station> stationsList;
	GpsCoordinates departure;
	GpsCoordinates arrival;
	double [][] graphe;
	
	public StationsGraphe(VelibSystem myVelib, GpsCoordinates departure, GpsCoordinates arrival) {
		super();
		this.myVelib = myVelib;
		this.stationsList = myVelib.getStationsList();
		this.arrival = arrival;
		this.departure = departure;
		int NMax = stationsList.size();
		this.graphe = new double [NMax+1][NMax+1];
		
	}
	
	/**
	 * Set the graph for the compilation of the shortest path with the distance from the stations to the 
	 * departure on the last line and the distance from the stations to the arrival on the last column
	 */
	public void setForShortestPath() {
		int NMax = stationsList.size();
		for (int i=0 ; i<NMax ; i++) {
			for (int j=0 ; j<NMax ; j++) {
				double dist = stationsList.get(i).getGpsCoord().distanceTo(stationsList.get(j).getGpsCoord());
				dist = dist*1000;
				dist = Math.round(dist);
				graphe[i][j] = dist/1000;
			}
			graphe[NMax][i] = Math.round(1000*departure.distanceTo(stationsList.get(i).getGpsCoord()))/1000;
			graphe[i][NMax] = Math.round(1000*arrival.distanceTo(stationsList.get(i).getGpsCoord()))/1000;
		}
		graphe[NMax][NMax]=0;
	}
	
	
	/**
	 * Set the graph for the compilation of the fastest path with the time from the stations to the 
	 * departure on the last line and the time from the stations to the arrival on the last column
	 */	
	public void setForFastestPath(BicycleType bikeType) {
		int NMax = stationsList.size();
		double elecSpeed = Math.round(20*1000000/60)/1000;
		double mechSpeed = Math.round(15*1000000/60)/1000;
		double walkingSpeed = Math.round(4*1000000/60)/1000;
		for (int i=0 ; i<NMax ; i++) {
			for (int j=0 ; j<NMax ; j++) {
				double dist = stationsList.get(i).getGpsCoord().distanceTo(stationsList.get(j).getGpsCoord());
				if (bikeType == BicycleType.Mechanical) {
					double time = dist*mechSpeed*1000;
					time = Math.round(time)/1000;
					graphe[i][j] = time;
				}
				else if (bikeType == BicycleType.Electrical) {
					double time = dist*elecSpeed*1000;
					time = Math.round(time)/1000;
					graphe[i][j] = time;
				}
			}
			graphe[NMax][i] = Math.round(1000*walkingSpeed*departure.distanceTo(stationsList.get(i).getGpsCoord()))/1000;
			graphe[i][NMax] = Math.round(1000*walkingSpeed*arrival.distanceTo(stationsList.get(i).getGpsCoord()))/1000;
		}	
	}
	
	
	
	@Override
	public String toString() {
		String str = "";
		for (int i=0; i<stationsList.size()+1;i++) {
			for (int j=0; j<stationsList.size()+1;j++) {
				if (this.graphe[i][j] == 0.0) {
					str += "0.000      ";
				}
				else if (this.graphe[i][j]<10){
					str += this.graphe[i][j] + "      ";
				}
				else if (this.graphe[i][j]>=10 && this.graphe[i][j]<100) {
					str += this.graphe[i][j] + "     ";
				}
				else if (this.graphe[i][j]<1000 && this.graphe[i][j]>=100) {
					str += this.graphe[i][j] + "    ";
				}
				else if (this.graphe[i][j]<10000 && this.graphe[i][j]>=1000) {
					str += this.graphe[i][j] + "   ";
				}
				else if (this.graphe[i][j]<100000 && this.graphe[i][j]>=10000) {
					str += this.graphe[i][j] + "  ";
				}
				else 
					str += this.graphe[i][j] + " ";
			}
		str += "\n";
		}
	return str;
	}
	
	public GpsCoordinates getDeparture() {
		return departure;
	}
	public void setDeparture(GpsCoordinates departure) {
		this.departure = departure;
	}
	public GpsCoordinates getArrival() {
		return arrival;
	}
	public void setArrival(GpsCoordinates arrival) {
		this.arrival = arrival;
	}
	public double[][] getGraphe() {
		return graphe;
	}
		
}
