package fr.ecp.IS1220.project.part1.planning;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Path;

public interface PlanningStrategy {
	/**
	 * @author Arnaud
	 * @param velib The VelibSystem you are currently working on 
	 * @param departure the departure coordinates of the customer
	 * @param arrival the destination coordinates
	 * @param bikeType the kind of bike the user wants to ride
	 * @return the path the customer as to follow acording to the policy he chose
	 */
	public Path getPath(VelibSystem velib, GpsCoordinates departure, GpsCoordinates arrival, BicycleType bikeType);
}