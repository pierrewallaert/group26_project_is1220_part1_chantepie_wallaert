package fr.ecp.IS1220.project.part1.planning;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleFactory;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.rides.CostStrategy;
import fr.ecp.IS1220.project.part1.rides.GuestElec;
import fr.ecp.IS1220.project.part1.rides.GuestMech;
import fr.ecp.IS1220.project.part1.rides.VLibrePlusElec;
import fr.ecp.IS1220.project.part1.rides.VLibrePlusMech;
import fr.ecp.IS1220.project.part1.rides.VLibreStandElec;
import fr.ecp.IS1220.project.part1.rides.VLibreStandMech;
import fr.ecp.IS1220.project.part1.rides.VMaxPlus;
import fr.ecp.IS1220.project.part1.rides.VMaxStand;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Observable;
import fr.ecp.IS1220.project.part1.util.Path;

public class RidePlan implements Observable{
	User user;
	BicycleType bikeType;
	int ID;
	static int uniqID;
	double startTime;
	double endTime;
	double duration;
	VelibSystem myVelib;
	GpsCoordinates departure, arrival;
	CostStrategy costStrat;
	double cost;
	PlanningStrategy planStrat;
	Path path;

	
	//Constructor
	public RidePlan(User user, BicycleType bikeType, VelibSystem myVelib, GpsCoordinates arrival, PlanningStrategy planStrat) {
		super();
		this.user = user;
		this.bikeType = bikeType;
		this.myVelib = myVelib;
		this.departure = user.getPosition();
		this.arrival = arrival;
		this.planStrat = planStrat;
		this.path = planStrat.getPath(myVelib, departure, arrival, bikeType);
		int bikeSpeed = 0;
		if (bikeType == BicycleType.Mechanical) {
			bikeSpeed = 15/60;
		}
		else {bikeSpeed = 20/60;}
		this.duration = this.departure.distanceTo(path.getStartStation().getGpsCoord())*60/4+path.getStartStation().getGpsCoord().distanceTo(path.getEndStation().getGpsCoord())*Math.pow(bikeSpeed, -1)
				+ path.getEndStation().getGpsCoord().distanceTo(this.arrival)*60/4;
		if (user.getType()==UserType.Guest ) {
			if (bikeType==BicycleType.Mechanical) {
				this.costStrat=new GuestMech(duration,user);
			}
			else {
				//The bike is electrical
				this.costStrat=new GuestElec(duration, user);
			}
			
		}
		else if(user.getType()==UserType.Registered) {
			RegisteredUser user1=(RegisteredUser) user;
			if (user1.getCardType()==CardType.VLibre){
				if (path.getEndStation().getStationType()==StationType.Standard) {

					if (bikeType==BicycleType.Mechanical) {
						this.costStrat=new VLibreStandMech(duration,user1);
					}
					else {
						this.costStrat=new VLibreStandElec(duration,user1);
					}
				}
				else {
					if (bikeType==BicycleType.Mechanical) {
						this.costStrat= new VLibrePlusMech(duration,user1);
						this.user.setTotalTimeCredit(this.user.getTotalTimeCredit()+5);
					}
					else {
						this.costStrat= new VLibrePlusElec(duration,user1);
						this.user.setTotalTimeCredit(this.user.getTotalTimeCredit()+5);
					}
				}
			}
			else {
				if (path.getEndStation().getStationType()==StationType.Standard) {
					
					this.costStrat=new VMaxStand(duration,user1);
				}
				else {
					this.costStrat= new VMaxPlus(duration,user1);
					this.user.setTotalTimeCredit(this.user.getTotalTimeCredit()+5);
				}
			}
		}
		this.ID = uniqID;
		uniqID += 1;
		this.startTime = 0;
		this.endTime = 0;
		this.cost = costStrat.cost().get(0);
		this.myVelib.addCurrentRidePlan(this);
		}
	

	//Getters and Setters
	
	public double getCost() {
		return cost;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BicycleType getBikeType() {
		return bikeType;
	}

	public void setBikeType(BicycleType bikeType) {
		this.bikeType = bikeType;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public VelibSystem getMyVelib() {
		return myVelib;
	}

	public void setMyVelib(VelibSystem myVelib) {
		this.myVelib = myVelib;
	}

	public GpsCoordinates getDeparture() {
		return departure;
	}

	public void setDeparture(GpsCoordinates departure) {
		this.departure = departure;
	}

	public GpsCoordinates getArrival() {
		return arrival;
	}

	public void setArrival(GpsCoordinates arrival) {
		this.arrival = arrival;
	}

	public CostStrategy getCostStrat() {
		return costStrat;
	}

	public void setCostStrat(CostStrategy costStrat) {
		this.costStrat = costStrat;
	}

	public PlanningStrategy getPlanStrat() {
		return planStrat;
	}

	public void setPlanStrat(PlanningStrategy planStrat) {
		this.planStrat = planStrat;
	}

	public Path getPath() {
		return path;
	}
	
	
	/**
	 * remove the RidePlan from the list of current RidePlan of the VelibSystem
	 */
	public void finish() {
		this.myVelib.getCurrentRidePlan().remove(this);
	}

	/**
	 * recalculate the path and update the user if there is any change
	 */
	@Override
	public void notifyObs() {
		Station FES = this.path.getEndStation();
		this.path = planStrat.getPath(myVelib, departure, arrival, bikeType);
		if (FES!=this.path.getEndStation()) {
		this.user.update(this);
		}
	} 
		
}
