package fr.ecp.IS1220.project.part1.planning;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Scanner;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.bicycles.MechanicalBicycle;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Moment;

public class VelibSystem {
	int ID;
	static int uniqID = 1;
	ArrayList<Station> stationsList;
	ArrayList<User> usersList;
	ArrayList<Bicycle> bikeList;
	ArrayList<RidePlan> currentRidePlan;
	public Calendar calendar; // creates a new calendar instance
	public Moment creation;
	String name;
	
	
	//Constructor
	public VelibSystem(String name, ArrayList<Station> stationsList, ArrayList<User> usersList, ArrayList<Bicycle> bikeList) {
		super();
		this.name = name;
		this.ID = uniqID;
		uniqID += 1;
		this.stationsList = stationsList;
		this.usersList = usersList;
		this.bikeList = bikeList;
		Date date = new Date();   // given date
		this.calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		this.calendar.setTime(date);
		this.currentRidePlan = new ArrayList<RidePlan>();
		this.creation=new Moment();
	}
	

	//Constructor initializing with setup <name>
	public VelibSystem(String name) {
		super();
		this.name = name;
		this.ID = uniqID;
		uniqID += 1;
		this.stationsList = new ArrayList<Station>();
		this.usersList = new ArrayList<User>();
		this.bikeList = new ArrayList<Bicycle>();
		this.currentRidePlan = new ArrayList<RidePlan>();
		ArrayList<GpsCoordinates> stationCoord = new ArrayList<GpsCoordinates>();
		int coordNum = 0;
		while (coordNum<10) {
			Random rand = new Random();
			int x = (int)(rand.nextInt()*4000);
			int y = (int)(rand.nextInt()*4000);
			GpsCoordinates coord = new GpsCoordinates(x, y);
			Boolean exist = false;
			for (GpsCoordinates g : stationCoord) {
				if(g.equals(coord)) {exist=true;}
			}
			if (exist.equals(false)) {
				stationCoord.add(coord);
				coordNum++;
			}
		}
		for (int i=0; i<10;i++) {
			Station s = new Station(stationCoord.get(i),10,this);
		}
		int occupSlots = 0;
		while (occupSlots<75) {
			occupSlots++;
			int randomNum = (int)(Math.random() * 100);
			int stationID = randomNum/10;
			int slotID = randomNum%10;
			if (occupSlots<=30) {
				this.getStationsList().get(stationID).getSlotList().get(slotID).setCurrentBike(new ElectricalBicycle());
			}
			else {
				this.getStationsList().get(stationID).getSlotList().get(slotID).setCurrentBike(new MechanicalBicycle());
			}
			
		}
		Date date = new Date();   // given date
		this.calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		this.calendar.setTime(date);
		this.creation=new Moment();
		
	}
	
	//Constructor for setup <name> <nstations> <nslots> <sidearea> <nbikes>
	public VelibSystem(String name, int nstations, int nslots, int sidearea, int nbikes) {
		super();
		this.name = name;
		this.ID = uniqID;
		uniqID += 1;
		this.stationsList = new ArrayList<Station>();
		this.usersList = new ArrayList<User>();
		this.bikeList = new ArrayList<Bicycle>();
		this.currentRidePlan = new ArrayList<RidePlan>();
		ArrayList<GpsCoordinates> stationCoord = new ArrayList<GpsCoordinates>();
		int coordNum = 0;
		while (coordNum<nstations) {
			Random rand = new Random();
			int x = (int)(rand.nextInt()*sidearea*1000);
			int y = (int)(rand.nextInt()*sidearea*1000);
			GpsCoordinates coord = new GpsCoordinates(x, y);
			Boolean exist = false;
			if (x>=0 && y>=0 && x<=sidearea*1000 && y<=sidearea*1000){
				for (GpsCoordinates g : stationCoord) {
					if(g.equals(coord)) {exist=true;}
				}
				if (exist.equals(false)) {
					stationCoord.add(coord);
					coordNum++;
				}
			}
		}
		for (int i=0; i<nstations;i++) {
			Station s = new Station(stationCoord.get(i),nslots,this);
		}
		int occupSlots = 0;
		while (occupSlots<nbikes) {
			occupSlots++;
			int randomNum = (int)(Math.random() * nstations*nslots);
			int stationID = randomNum/nstations;
			int slotID = randomNum%nslots;
			if (occupSlots<=0.3*nbikes) {
				this.getStationsList().get(stationID).getSlotList().get(slotID).setCurrentBike(new ElectricalBicycle());
			}
			else {
				this.getStationsList().get(stationID).getSlotList().get(slotID).setCurrentBike(new MechanicalBicycle());
			}
			
		}
		Date date = new Date();   // given date
		this.calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		this.calendar.setTime(date);
		this.creation=new Moment();

		
		
	}
	
	
	/*
	 * public VelibSystem(String name) { super(); this.name = name; this.ID =
	 * uniqID; uniqID += 1; this.bikeList = new ArrayList<Bicycle>();
	 * this.stationsList = new ArrayList<Station>(); this.usersList = new
	 * ArrayList<User>(); Date date = new Date(); // given date this.calendar =
	 * GregorianCalendar.getInstance(); // creates a new calendar instance
	 * this.calendar.setTime(date); this.currentRidePlan = new
	 * ArrayList<RidePlan>(); for (int i=0; i<10;i++) { this.stationsList.add(new
	 * Station(new GpsCoordinates(i/5,i%5),10,this)); }
	 * 
	 * }
	 */
	
	
	//Constructor for the use case scenario
	public VelibSystem(String name, int N , int M, double occupProportion, double electricalProportion, ArrayList<GpsCoordinates> stationCoord) {
		super();
		this.name = name;
		Boolean correct;
		if(N==stationCoord.size()) {
			correct = true;
			this.ID = uniqID;
			uniqID += 1;
			this.stationsList = new ArrayList<Station>();
			this.usersList = new ArrayList<User>();
			this.bikeList = new ArrayList<Bicycle>();
			this.currentRidePlan = new ArrayList<RidePlan>();
			this.creation=new Moment();

			for (int i=0; i<N;i++) {
				this.stationsList.add(new Station(stationCoord.get(i),Math.round(M/N),this));
				for (int j=0; j<occupProportion*this.stationsList.get(i).getNumberOfSlots();j++) {
					if (j<electricalProportion*occupProportion*Math.round(M/N)) {
						this.stationsList.get(i).getSlotList().get(j).setOccupation(ParkingSlotOccupation.Occupied, new ElectricalBicycle());
					}
					else {
						this.stationsList.get(i).getSlotList().get(j).setOccupation(ParkingSlotOccupation.Occupied, new MechanicalBicycle());
					}
				}
			}
		
			Date date = new Date();   // given date
			this.calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
			this.calendar.setTime(date);
		}
		else {
			correct = false;
			System.err.println("Wrong amount of coordinates");
		}
	}
	
	
	
	//Getters and Setters
	public ArrayList<Station> getStationsList() {
		return stationsList;
	}

	public void setStationsList(ArrayList<Station> stationsList) {
		this.stationsList = stationsList;
	}
	
	public void addStation(Station station) {
		this.stationsList.add(station);
	}
	
	public void addStation(ArrayList<Station> stationsList) {
		for (Station station : stationsList) {
			this.stationsList.add(station);
		}
	}

	public ArrayList<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(ArrayList<User> usersList) {
		this.usersList = usersList;
	}
	
	public void addUser(User user) {
		this.usersList.add(user);
	}
	
	public void addUser(ArrayList<User> usersList) {
		for (User user : usersList) {
			this.usersList.add(user);
		}
	}

	public ArrayList<Bicycle> getBikeList() {
		return bikeList;
	}

	public void setBikeList(ArrayList<Bicycle> bikeList) {
		this.bikeList = bikeList;
	}
	
	public void addBicycle(Bicycle bike) {
		this.bikeList.add(bike);
	}
	
	public void addBicycle(ArrayList<Bicycle> bikeList) {
		for (Bicycle bike : bikeList) {
			this.bikeList.add(bike);
		}
	}
	
	public ArrayList<RidePlan> getCurrentRidePlan() {
		return currentRidePlan;
	}

	public void setCurrentRidePlan(ArrayList<RidePlan> currentRidePlan) {
		this.currentRidePlan = currentRidePlan;
	}
	
	public void addCurrentRidePlan(ArrayList<RidePlan> currentRidePlan) {
		for (RidePlan cRP : currentRidePlan) {
		this.currentRidePlan.add(cRP);
		}
	}
	
	public void addCurrentRidePlan(RidePlan currentRidePlan) {
		this.currentRidePlan.add(currentRidePlan);
	}

	
	
	public int getID() {
		return ID;
	}
	
	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Sorts stations by activity
	 * @return an ArrayList of the stations which are sorted by activity
	 */
	public ArrayList<Station> getMostUsedStation() {
		ArrayList<Station> tri=new ArrayList<Station>();
		for(Station station:stationsList) {
			tri.add(station);
		}
		
				for(int i=0;i<tri.size();i++) {
					int max=tri.get(i).getActivity();
					int index=i;
					for(int j=i+1;j<tri.size();j++) {
						int value=tri.get(j).getActivity();
						if(value>max) {
							max=value;
							index=j;
						}
					}
					Station maximal=tri.get(index);
					tri.set(index, tri.get(i));
					tri.set(i, maximal);
				}
		return tri;
		
	}

	/**
	 * Sorts stations by occupation
	 * @return an ArrayList of the stations which are sorted by occupation
	 */
	public ArrayList<Station> getLeastOccupiedStation(Moment ts, Moment te) {
		ArrayList<Station> tri=new ArrayList<Station>();
		for(Station station:stationsList) {
			tri.add(station);
		}

				for(int i=0;i<tri.size();i++) {
					double max=tri.get(0).getOccupationRate(ts, te);
					int index=0;
					for(int j=1;j<=i;j++) {
						double value=tri.get(j).getOccupationRate(ts, te);
						if(value>max) {
							max=value;
							index=j;
						}
					}
					Station maximal=tri.get(index);
					tri.set(index, tri.get(tri.size()-1-i));
					tri.set(tri.size()-1-i, maximal);
				}
		return tri;
		
		
	}
	public Station getStationFromID(int ID) {
		for (Station station:this.stationsList) {
			if (station.getID()==ID) {
				return(station);
			}
		}
		System.err.println("Unknown station ID");
		return(null);
	}
	public User getUserFromID(int ID) {
		for (User user:this.usersList) {
			if (user.getID()==ID) {
				return(user);
			}
		}
		System.err.println("Unknown user ID");
		return(null);
	}
	


	public static void main(String[] args) {
		VelibSystem myVelib = new VelibSystem("myVelib");
		System.out.println(myVelib.getStationsList());
	}

	
	
}
