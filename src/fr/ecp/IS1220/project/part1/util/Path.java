package fr.ecp.IS1220.project.part1.util;

import fr.ecp.IS1220.project.part1.stations.Station;


/**
 * Return the start and end station
 * @author Arnaud
 *
 */
public class Path {
	Station startStation;
	Station endStation;
	
	public Path(Station startStation, Station endStation) {
		super();
		this.startStation = startStation;
		this.endStation = endStation;
	}

	public Path() {
		super();
	}
	
	public Station getStartStation() {
		return startStation;
	}

	public void setStartStation(Station startStation) {
		this.startStation = startStation;
	}

	public Station getEndStation() {
		return endStation;
	}

	public void setEndStation(Station endStation) {
		this.endStation = endStation;
	}

	@Override
	public String toString() {
		return "The path goes from \n\n\t" + startStation + " \n\nto\n\n\t" + endStation;
	}
	
	
	
	
}
