
package fr.ecp.IS1220.project.part1.util;

import java.util.Calendar;

import java.util.Date;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
/**
 * 
 * @author pierrewallaert
 *Representing the time of the computer
 *@params time: long representing the time in millis seconds
 */
public class Moment {

	public long time;
	
	
	public Moment() {
		this.time=Calendar.getInstance().getTimeInMillis();
	}
	
	public Moment(long time) {
		
		this.time = time;
	}
		
	

	

	@Override
	public String toString() {
		return "Moment [time=" + time + "]";
	}
	/**
	 * 
	 * @author pierrewallaert
	 *Computing the time between mom and this
	 *@params mom: Moment of the start
	 *@return delay in millis
	 */
	public long delay(Moment mom) {
		return(time-mom.time);
	}
	/**
	 * 
	 * @author pierrewallaert
	 *if this is after mom
	 *@params mom: Moment of the start
	 *@return boolean
	 */
	public boolean after(Moment mom) {
		//if this is after mom
		return(time>=mom.time);
	}
	/**
	 * 
	 * @author pierrewallaert
	 *if this is before mom
	 *@params mom: Moment of the start
	 *@return boolean
	 */
	public boolean before(Moment mom) {
		//if this is after mom
		return(time<=mom.time);	}

}
