package fr.ecp.IS1220.project.part1.util;

public interface Observable {
	/**
	 * Notify the users of any changes in there planning
	 */
	public void notifyObs();
}
