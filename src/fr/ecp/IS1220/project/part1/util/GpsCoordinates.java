package fr.ecp.IS1220.project.part1.util;

import fr.ecp.IS1220.project.part1.rides.CostStrategy;

/**
 * Return the GPS Coordinates of a place
 * @author Arnaud
 * @param latitude Latitude of the place, positive if oriented North, negative if oriented South
 * @param longitude Longitude of the place, positive if oriented East, negative if oriented West
 * @return the GPS Coordinates
 */

public class GpsCoordinates extends Object {
	
	//Attributes
	protected double latitude;
	protected double longitude;
	
	//Constructor
	public GpsCoordinates(double latitude, double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	//Getters and Setters
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return latitude + " N, " + longitude + " E";
	}
	
	public double planeDistanceTo(GpsCoordinates point) {
		double deltaX = point.longitude - this.longitude;
		double deltaY = point.latitude - this.latitude;
		double distance = Math.sqrt(Math.pow(deltaX, 2)+Math.pow(deltaY, 2));		
		return distance;	
	}
	
	public double distanceTo(GpsCoordinates point) {
		double x = (point.longitude - this.longitude)*Math.cos((this.latitude+point.latitude)/2);
		double y = point.latitude - this.latitude;
		double distance = 1.852*Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2));
		
		
		return distance;	
	}
	
	


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GpsCoordinates) {
			GpsCoordinates gpsobj = (GpsCoordinates) obj;
			if (this.latitude == gpsobj.latitude && this.longitude == gpsobj.longitude) {
				return true;
			}
		}
		return false;
	}
	
	
}
