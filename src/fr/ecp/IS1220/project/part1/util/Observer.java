package fr.ecp.IS1220.project.part1.util;

import fr.ecp.IS1220.project.part1.planning.RidePlan;

public interface Observer {
	/**
	 * Inform the user of the changes in the ridePlan he is currently following
	 * @param rP the current ridePlan
	 */
	public void update(RidePlan rP);
}
