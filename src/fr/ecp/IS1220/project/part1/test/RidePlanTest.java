package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleFactory;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.planning.MinimalWalkingDistance;
import fr.ecp.IS1220.project.part1.planning.PlanningStrategy;
import fr.ecp.IS1220.project.part1.planning.RidePlan;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.users.Guest;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

class RidePlanTest {
	VelibSystem myVelib = new VelibSystem("myVelib");
	BicycleType bikeType = BicycleType.Electrical;
	GpsCoordinates depart = new GpsCoordinates(0.000,0.000);
	GpsCoordinates arrivee = new GpsCoordinates(100, 100);
	PlanningStrategy strat = new MinimalWalkingDistance();
	User user = new Guest("Name", "surname", depart);
	


	@Test
	void testTurnedOffline() {
		System.out.println("Test 1");
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, BicycleFactory.createBicycle(bikeType));
		myVelib.getStationsList().get(0).getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		RidePlan rP = new RidePlan(user, bikeType, myVelib, arrivee, strat);
		rP.getPath().getEndStation().setStationStatus(StationStatus.Offline);
		rP.notifyObs();
		assert rP.getPath().getEndStation().equals(myVelib.getStationsList().get(3));
	}
	
	@Test
	void testAllSlotsOccupied() {
		System.out.println("Test 1");
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, BicycleFactory.createBicycle(bikeType));
		myVelib.getStationsList().get(0).getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		RidePlan rP = new RidePlan(user, bikeType, myVelib, arrivee, strat);
		for (int k=0; k<rP.getPath().getEndStation().getNumberOfSlots();k++) {
			rP.getPath().getEndStation().getSlotList().get(k).setOccupation(ParkingSlotOccupation.Occupied, new ElectricalBicycle());
		}
		rP.notifyObs();
		assert rP.getPath().getEndStation().equals(myVelib.getStationsList().get(3));
	}

}