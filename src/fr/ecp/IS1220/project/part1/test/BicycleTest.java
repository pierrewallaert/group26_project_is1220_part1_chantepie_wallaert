package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.bicycles.MechanicalBicycle;

class BicycleTest {

	@Test
	void testElectrical() {
		Bicycle bike = new ElectricalBicycle();
		assert (bike.getType()==BicycleType.Electrical);
	}

	@Test
	void testMechanical() {
		Bicycle bike = new MechanicalBicycle();
		assert (bike.getType()==BicycleType.Mechanical);
	}
	
	@Test
	void testID() {
		Bicycle bike = new MechanicalBicycle();
		System.out.println(bike.getID());
		assert (bike.getID()==1);
	}

}
