package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.planning.PlanningStrategy;
import fr.ecp.IS1220.project.part1.planning.PreferPlus;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Path;

class PreferPlusTest {
	VelibSystem myVelib = new VelibSystem("myVelib");
	Path path = new Path();
	Bicycle bike = new ElectricalBicycle();
	GpsCoordinates depart = new GpsCoordinates(0.000,0.000);
	GpsCoordinates arrivee = new GpsCoordinates(100, 100);
	PlanningStrategy strat = new PreferPlus();

	@Test
	void TestPrefer() {
		System.out.println("Test 1");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(86, 92),5,myVelib));
		myVelib.getStationsList().get(3).setStationType(StationType.Plus);
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(3));
	}
	
	@Test
	void TestClosestPrefer() {
		System.out.println("Test 2");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(86,91),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(86,50),5,myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(86, 92),5,myVelib));
		myVelib.getStationsList().get(2).setStationType(StationType.Plus);
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(2));
	}
	
	
}
