package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleFactory;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Moment;

class SlotsTest {

	
	  @Test void test() throws InterruptedException { 
		  VelibSystem myVelib= new VelibSystem("test");
	  
	  User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new
	  GpsCoordinates(0, 1) );
	  RegisteredUser
	  userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin",
	  new GpsCoordinates(2, 0), CardType.VLibre); RegisteredUser
	  userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile",
	  new GpsCoordinates(2, 0), CardType.VMax); Bicycle
	  bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical); Bicycle
	  bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
	  myVelib.getStationsList().get(0).getSlotList().get(0).setOccupation(ParkingSlotOccupation.Occupied,
	  bikeelec);
	  myVelib.getStationsList().get(0).getSlotList().get(2).setOccupation(ParkingSlotOccupation.Free,
			  bikeelec);
	  long time=Calendar.getInstance().getTimeInMillis();
	  System.out.println("time="+time);
	  userLibre.rent( myVelib.getStationsList().get(0).getSlotList().get(0));
	  Thread.sleep(6000);
	  
	  userLibre.endRent( myVelib.getStationsList().get(0).getSlotList().get(2));
	  Thread.sleep(6000); 
	  userLibre.rent( myVelib.getStationsList().get(0).getSlotList().get(2));
	  Thread.sleep(6000); 
	  userLibre.endRent(
			  myVelib.getStationsList().get(0).getSlotList().get(0));
	  System.out.println(myVelib.getStationsList().get(0).getSlotList().get(2).occupiedTime.get(0)[0].time+""+myVelib.getStationsList().get(0).getSlotList().get(2).occupiedTime.get(0)[1].time);
	  System.out.println("occupation:"+myVelib.getStationsList().get(0)
			  .getOccupationRate());
	  System.out.println(myVelib.getMostUsedStation());
	  System.out.println(myVelib.getStationsList().get(0).getActivity());
	  System.out.println(myVelib.getStationsList().get(1).getActivity());
	  }
	
	/*
	 * @Test void test2() { VelibSystem myVelib= new VelibSystem("test"); long time
	 * = Calendar.getInstance().getTimeInMillis(); Moment m1=new Moment(time);
	 * Moment m2 =new Moment(time+60000); double delay=m2.delay(m1);
	 * assertEquals(delay,60000);
	 * 
	 * 
	 * }
	 * 
	 * @Test void test3() throws InterruptedException {
	 * Calendar.getInstance().setTimeInMillis(0); long
	 * calendar=Calendar.getInstance().getTimeInMillis(); Thread.sleep(0); long
	 * calendar2=Calendar.getInstance().getTimeInMillis();
	 * System.out.println(calendar); System.out.println(calendar2); }
	 */

}
