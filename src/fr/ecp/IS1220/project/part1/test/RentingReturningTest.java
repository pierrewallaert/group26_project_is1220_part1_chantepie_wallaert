package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleFactory;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.planning.MinimalWalkingDistance;
import fr.ecp.IS1220.project.part1.planning.PlanningStrategy;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

class RentingReturningTest {
	public VelibSystem velib = new VelibSystem("myVelib");
	Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
	Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
	//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
	User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
	RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
	RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
	Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
	Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
	
	PlanningStrategy planStrat = new MinimalWalkingDistance();
	ArrayList<Station> listStat= new ArrayList<Station>();
	
	@Test
	void rentalBikeNobike() {
		userGuest.rent( stationStand.getSlotList().get(0));
		assertEquals(0,userGuest.getRideNumber());
	}
	@Test
	void rentalBike() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		userGuest.rent(stationStand.getSlotList().get(1));
		System.out.println("null");
		System.out.println(userGuest);
		assertEquals(1,userGuest.getRideNumber());
	
		
	}
	@Test
	void returnBike() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		userGuest.rent( stationStand.getSlotList().get(1));
		userGuest.endRent( stationStand.getSlotList().get(1));
		
		assertEquals(stationStand.getSlotList().get(1).getOccupation(), ParkingSlotOccupation.Occupied);
		assertEquals(userGuest.getRides().get(0).getEndStation(),stationStand);
	}
	@Test
	void rentBikeStationOutService() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied,bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.Offline);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		userGuest.rent( stationStand.getSlotList().get(1));
		assertEquals(0,userGuest.getRideNumber());
		
	}
	
	
	@Test
	void  returnBikeStationOut(){
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		userGuest.rent( stationStand.getSlotList().get(1));
		stationStand.setStationStatus(StationStatus.Offline);
		userGuest.endRent( stationStand.getSlotList().get(1));
		
		assertEquals(stationStand.getSlotList().get(1).getOccupation(), ParkingSlotOccupation.Free);
		assertEquals(userGuest.getRides().get(0).getEndStation(),null);
	}
	@Test
	void rentBikeAlreadyRideInProgress() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		

		
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied,bikeelec);
		stationStand.getSlotList().get(2).setOccupation(ParkingSlotOccupation.Occupied,bikemech);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.getSlotList().get(2).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		stationStand.getSlotList().get(2).setCurrentBike(bikemech);
		userGuest.rent( stationStand.getSlotList().get(1));
		userGuest.rent( stationStand.getSlotList().get(2));
		assertEquals(1,userGuest.getRideNumber());
		
	}
	@Test
	void returnBikeNoRideInProgress() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Free,bikeelec);
		stationStand.getSlotList().get(2).setOccupation(ParkingSlotOccupation.Free,bikemech);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.getSlotList().get(2).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		
		userGuest.endRent( stationStand.getSlotList().get(1));
		
		assertEquals(0,userGuest.getRideNumber());
		
	}
	@Test
	void returnBikeAndRent() {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationStand.getSlotList().get(1).setCurrentBike(bikeelec);
		userGuest.rent(stationStand.getSlotList().get(1));
		userGuest.endRent( stationStand.getSlotList().get(1));
		userGuest.rent( stationStand.getSlotList().get(1));
		
		assertEquals(2,userGuest.getRideNumber());
	}

	@Test
	void returnBikeAndRentLibre() throws InterruptedException {
		Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15,velib);
		Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15,velib);
		//Changer la definition de la creation des stations pour qu'elles soient automatiquement dans VelibSystem
		User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
		RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
		RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
		Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
		Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
		
		PlanningStrategy planStrat = new MinimalWalkingDistance();
		ArrayList<Station> listStat= new ArrayList<Station>();
		stationStand.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bikeelec);
		stationStand.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationStand.setStationStatus(StationStatus.OnService);
		stationPlus.getSlotList().get(1).setCurrentBike(bikeelec);
		stationPlus.getSlotList().get(1).setOccupation(ParkingSlotOccupation.Free, bikeelec);
		stationPlus.getSlotList().get(1).setStatus(ParkingSlotStatus.Available);
		stationPlus.setStationStatus(StationStatus.OnService);
		
		userLibre.rent( stationStand.getSlotList().get(1));
		Thread.sleep(65000);
		userLibre.endRent( stationPlus.getSlotList().get(1));
		userLibre.rent( stationPlus.getSlotList().get(1));
		
		assertEquals(2,userLibre.getRideNumber());
		assertEquals(5,userLibre.getTotalTimeCredit());
	}

}
