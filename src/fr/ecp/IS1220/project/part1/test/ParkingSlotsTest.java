package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.ParkingSlots;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

class ParkingSlotsTest extends TestCase{
	Bicycle bike = new ElectricalBicycle();
	VelibSystem myVelib = new VelibSystem("myVelib");
	Station s = new Station(new GpsCoordinates(0,1),4,myVelib);
	
	@Test
	void testStatusInitialisation() {
		ParkingSlots PS = new ParkingSlots(s);
		ParkingSlotOccupation occup = PS.getOccupation();
		occup.equals(ParkingSlotOccupation.Free);
	}
	
	@Test
	void testStatusChangingToOccupied() {
		ParkingSlots PS = new ParkingSlots(s);
		PS.setOccupation(ParkingSlotOccupation.Occupied, bike);
		ParkingSlotOccupation occup = PS.getOccupation();
		occup.equals(ParkingSlotOccupation.Occupied);
	}
	
	@Test
	void testStatusChangingToOutofOrder() {
		ParkingSlots PS = new ParkingSlots(s);
		PS.setStatus(ParkingSlotStatus.OutofOrder);
		ParkingSlotStatus status = PS.getStatus();
		status.equals(ParkingSlotStatus.OutofOrder);
	}
	

}
