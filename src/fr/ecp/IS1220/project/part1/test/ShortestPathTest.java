package fr.ecp.IS1220.project.part1.test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.planning.PlanningStrategy;
import fr.ecp.IS1220.project.part1.planning.ShortestPath;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Path;

class ShortestPathTest {
	VelibSystem myVelib = new VelibSystem("myVelib");
	Path path = new Path();
	Bicycle bike = new ElectricalBicycle();
	GpsCoordinates depart = new GpsCoordinates(0.000,0.000);
	GpsCoordinates arrivee = new GpsCoordinates(100, 100);
	PlanningStrategy strat = new ShortestPath();
	


	@Test
	void testWorking() {
		System.out.println("Test 1");
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(4));
		
	}
	
	@Test 
	void testNoBike() {
		System.out.println("Test 2");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert (path==null);
		
	}
	
	@Test
	void testNoBikeStart() {
		System.out.println("Test 3");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(1).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getStartStation().equals(myVelib.getStationsList().get(1));
	}
	
	@Test
	void testNoSlotEnd() {
		System.out.println("Test 4");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(1).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		for (int i =0; i<5;i++) {
			myVelib.getStationsList().get(4).getSlotList().get(i).setOccupation(ParkingSlotOccupation.Occupied, bike);
		}
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(3));
	}
	
	@Test
	void testStardOffline() {
		System.out.println("Test 5");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(1).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		myVelib.getStationsList().get(0).setStationStatus(StationStatus.Offline);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getStartStation().equals(myVelib.getStationsList().get(1));
	}

	@Test
	void testSEndOffline() {
		System.out.println("Test 6");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		myVelib.getStationsList().get(4).setStationStatus(StationStatus.Offline);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(3));
	}
	
	@Test
	void testAllStationsOfflineExceptOneWithoutBike() {
		System.out.println("Test 7");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		for (int i=0; i<4; i++) {
			myVelib.getStationsList().get(i).setStationStatus(StationStatus.Offline);
		}
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert (path == null);
	}
	
	@Test
	void testAllStationsOfflineExceptOneWithBike() {
		System.out.println("Test 8");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		for (int i=0; i<4; i++) {
			myVelib.getStationsList().get(i).setStationStatus(StationStatus.Offline);
		}
		myVelib.getStationsList().get(4).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert (path == null);
	}

	@Test
	void testLastStationAllSlotsOccupied() {
		System.out.println("Test 9");
		myVelib.setStationsList(new ArrayList<Station>());
		myVelib.addStation(new Station(new GpsCoordinates(0,0),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(7,25),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(50,72),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(85,91),5, myVelib));
		myVelib.addStation(new Station(new GpsCoordinates(99, 99),5, myVelib));
		for (int i=0; i<5; i++) {
			myVelib.getStationsList().get(4).getSlotList().get(i).setOccupation(ParkingSlotOccupation.Occupied, bike);
		}
		myVelib.getStationsList().get(0).getSlotList().get(1).setOccupation(ParkingSlotOccupation.Occupied, bike);
		path = strat.getPath(myVelib, depart, arrivee, bike.getType());
		assert path.getEndStation().equals(myVelib.getStationsList().get(3));
	}
}
