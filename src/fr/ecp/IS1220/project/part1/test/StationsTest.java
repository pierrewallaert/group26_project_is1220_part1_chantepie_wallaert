package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.bicycles.ElectricalBicycle;
import fr.ecp.IS1220.project.part1.bicycles.MechanicalBicycle;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.ParkingSlots;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationStatus;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import junit.framework.TestCase;

class StationsTest extends TestCase {
	VelibSystem myVelib = new VelibSystem("myVelib");
	/**
	 * Test if the AddSlot method works
	 */
	@Test
	void testAddSlot() {
		Station S = new Station(new GpsCoordinates(120.5,12), StationType.Plus, 10, myVelib);
		S.setStationStatus(StationStatus.Offline);
		S.addSlot();
		int M = S.getSlotList().size();
		assertTrue(M == 11);
		
	}
	
	/**
	 * test if creating an Offline station puts terminal and ParkingSlots Off
	 */
	@Test
	void testOffline() {
		Station S = new Station(new GpsCoordinates(120.5,12), 10, myVelib);
		S.setStationStatus(StationStatus.Offline);
		int rand = (int) Math.random()*(10)+1;
		ArrayList<ParkingSlots> SL = S.getSlotList();
		ParkingSlots PS = SL.get(rand);
		PS.getOccupation().equals(ParkingSlotOccupation.Free);	
	}
	
	@Test
	void testNumberType() {
		Station S = new Station(new GpsCoordinates(120.5,12), 10, myVelib);
		for (int i=0; i< S.getNumberOfSlots();i++) {
			S.getSlotList().get(i).setCurrentBike(new ElectricalBicycle());
		}
		S.getSlotList().get(2).setCurrentBike(new MechanicalBicycle());
		assert (S.getNumberOfSlotsWichAre(BicycleType.Electrical)==9);
	}
	
	@Test
	void tesNumberOccup() {
		Station S = new Station(new GpsCoordinates(120.5,12), 10, myVelib);
		for (int i=0; i< S.getNumberOfSlots();i++) {
			S.getSlotList().get(i).setCurrentBike(new ElectricalBicycle());
		}
		assert (S.getNumberOfSlotsWichAre(ParkingSlotOccupation.Occupied)==10);
	}
	
	@Test
	void tesNumberstatus() {
		Station S = new Station(new GpsCoordinates(120.5,12), 10, myVelib);
		for (int i=0; i< S.getNumberOfSlots();i++) {
			S.getSlotList().get(i).setStatus(ParkingSlotStatus.OutofOrder);
		}
		assert (S.getNumberOfSlotsWichAre(ParkingSlotStatus.OutofOrder)==10);
	}
	

}
