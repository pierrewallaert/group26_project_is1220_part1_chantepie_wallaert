package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.Terminal;
import fr.ecp.IS1220.project.part1.stations.TerminalStatus;
import junit.framework.TestCase;

class TerminalTest extends TestCase{
	VelibSystem myVelib = new VelibSystem("myVelib");
	Station station = new Station();
	
	@Test
	void testTerminalStatus() {
		Terminal T = new Terminal(station);
		TerminalStatus S = T.getStatus();
		S.equals(TerminalStatus.Available);
	}
	
	@Test
	void testTerminalUnvailable() {
		Terminal T = new Terminal(station);
		TerminalStatus S = T.getStatus();
		T.setStatus(TerminalStatus.Unavailable);
		S.equals(TerminalStatus.Unavailable);
	}

}
