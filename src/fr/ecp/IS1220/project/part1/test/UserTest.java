package fr.ecp.IS1220.project.part1.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.Guest;
import fr.ecp.IS1220.project.part1.users.LibreClient;
import fr.ecp.IS1220.project.part1.users.MaxClient;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

class UserTest {

	@Test
	void testGuest() {
		User user = new Guest("Wallaert", "Pierre", new GpsCoordinates(1.254,7.548));
		assert (user.getType() == UserType.Guest);
	}
	
	@Test 
	void testLibre1() {
		User user = new LibreClient("Wallaert", "Pierre", new GpsCoordinates(1.254,7.548));
		assert (user.getType() == UserType.Registered);
	}
	
	@Test 
	void testLibre2() {
		User user = new LibreClient("Wallaert", "Pierre", new GpsCoordinates(1.254,7.548));
		assert (((RegisteredUser) user).getCardType() == CardType.VLibre);
	}
	
	@Test 
	void testMax1() {
		User user = new MaxClient("Wallaert", "Pierre", new GpsCoordinates(1.254,7.548));
		assert (user.getType() == UserType.Registered);
	}
	
	@Test 
	void testMax2() {
		User user = new MaxClient("Wallaert", "Pierre", new GpsCoordinates(1.254,7.548));
		assert (((RegisteredUser) user).getCardType() == CardType.VMax);
	}

}
