package fr.ecp.IS1220.project.part1.rides;

import java.util.ArrayList;
/**
 * Interface for calculating cost of rides
 * @author Pierre
 * 
 * @param type the type of bicycle : Mechanical or Electrical 
 */
public interface CostStrategy {
	
	/**
	 * method returning the cost the user has to pay and the potential new time balance
	 * @author Pierre
	
	 * @return ArrayList with price first and time balance then
	 */
	public ArrayList<Double> cost();
	
	
}
