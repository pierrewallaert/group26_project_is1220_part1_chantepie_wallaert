package fr.ecp.IS1220.project.part1.rides;

import java.util.ArrayList;

import fr.ecp.IS1220.project.part1.users.User;

/**
 * Class defining CostStrategy for a Guest user an electric bike
 * @author Pierre
 * @param duration: duration of the ride
 * @param user: user who performed the ride and will be charged 
 */
public class GuestElec implements CostStrategy {
	double duration;
	User user;
	
	//constructor
	public GuestElec(double duration, User user) {
		this.duration= duration;
		this.user= user;
	}
	
	@Override
	public ArrayList<Double> cost() {
		ArrayList<Double> C=new ArrayList<Double>();
		
		
		C.add(2*duration/60);
		C.add(0.0);
		return (C);
	}


	


}
