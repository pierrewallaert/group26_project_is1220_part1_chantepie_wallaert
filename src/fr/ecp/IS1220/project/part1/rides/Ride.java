package fr.ecp.IS1220.project.part1.rides;

import fr.ecp.IS1220.project.part1.users.*;

import java.util.Calendar;

//import java.time.Duration;

import fr.ecp.IS1220.project.part1.bicycles.*;
import fr.ecp.IS1220.project.part1.planning.RidePlan;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.*;

/**
 * Class representing a ride performed
 * @author Pierre
 * @param user: user who performed the ride and will be charged 
 * @param bike: Bicycle with which is performed the ride
 * @param startStation: Start of the ride
 * @param endStation: End of the ride if ride is over
 * @param cost: CostStrategy object representing how is calculated the cost of the ride
 * @param ID: number of the ride
 * @param duration: how long the user used the bike
 * @param startTime: when the ride started in millis
 * @param endTime: when the ride ended in millis
 * @param velib: velib system in which the ride is performed
 */
public class Ride {
	User user;
	Bicycle bike;
	Station startStation, endStation;
	public CostStrategy cost;
	int ID;
	static int uniqID;
	double duration;
	long startTime;
	long endTime;
	VelibSystem velib;
	
	//Constructor
	public Ride(User user, Bicycle bike, Station startStation, Station endStation,  double duration,VelibSystem velib) {
		super();
		this.user = user;
		this.bike = bike;
		this.startStation = startStation;
		this.endStation = endStation;
		this.velib=velib;
		if (user.getType()==UserType.Guest ) {
			if (bike.getType()==BicycleType.Mechanical) {
				this.cost=new GuestMech(duration,user);
			}
			else {
				//The bike is electrical
				this.cost=new GuestElec(duration, user);
			}
			
		}
		else if(user.getType()==UserType.Registered) {
			RegisteredUser user1=(RegisteredUser) user;
			if (user1.getCardType()==CardType.VLibre){
				if (endStation.getStationType()==StationType.Standard) {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost=new VLibreStandMech(duration,user1);
					}
					else {
						this.cost=new VLibreStandElec(duration,user1);
					}
				}
				else {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost= new VLibrePlusMech(duration,user1);
					}
					else {
						this.cost= new VLibrePlusElec(duration,user1);
					}
					
				}
			}
			else {
				if (endStation.getStationType()==StationType.Standard) {
					
					this.cost=new VMaxStand(duration,user1);
					
					
				}
				else {
					
					this.cost= new VMaxPlus(duration,user1);
					
					
					
				}
				
			}
		}
		this.ID = uniqID;
		uniqID += 1;
		this.duration = duration;
	}
	public Ride(User user, Bicycle bike, Station startStation, VelibSystem velib) {
		super();
		this.user = user;
		this.bike = bike;
		this.startStation = startStation;
		this.velib=velib;
		
		
		this.ID = uniqID;
		uniqID += 1;
		this.startTime= Calendar.getInstance().getTimeInMillis(); 
		
		
		
	}
	

	//Getters and Setters
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Bicycle getBike() {
		return bike;
	}

	public void setBike(Bicycle bike) {
		this.bike = bike;
	}

	public Station getStartStation() {
		return startStation;
	}

	public void setStartStation(Station startStation) {
		this.startStation = startStation;
	}

	public Station getEndStation() {
		return endStation;
	}

	public void setEndStation(Station endStation) {
		this.endStation = endStation;
	}

	
	public int getID() {
		return ID;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
	/**
	 * Method ending the ride with the current time and given station
	 * @author Pierre
	 * 
	 * @param station: Station where the ride is ended
	 * @return null
	 */
	public void endRide(Station station) {
		this.endTime= Calendar.getInstance().getTimeInMillis(); 
		
		this.endStation=station;
		for (RidePlan rP : this.velib.getCurrentRidePlan()) {
			if (rP.getBikeType() == this.bike.getType()&&rP.getPath().getEndStation()==this.endStation&&this.user == rP.getUser()) {
				rP.finish();
			}
		}
		
		this.duration=(endTime-startTime)/60000;
		user.setTotalSpentTime(user.getTotalSpentTime()+this.duration);
		if (user.getType()==UserType.Guest ) {
			if (bike.getType()==BicycleType.Mechanical) {
				this.cost=new GuestMech(duration,user);
			}
			else {
				//The bike is electrical
				this.cost=new GuestElec(duration, user);
			}
			
		}
		else if(user.getType()==UserType.Registered) {
			RegisteredUser user1=(RegisteredUser) user;
			if (user1.getCardType()==CardType.VLibre){
				if (endStation.getStationType()==StationType.Standard) {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost=new VLibreStandMech(duration,user1);
					}
					else {
						this.cost=new VLibreStandElec(duration,user1);
					}
				}
				else {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost= new VLibrePlusMech(duration,user1);
					}
					else {
						this.cost= new VLibrePlusElec(duration,user1);
					}
					
				}
			}
			else {
				if (endStation.getStationType()==StationType.Standard) {
					
					this.cost=new VMaxStand(duration,user1);
					
					
				}
				else {
					
					this.cost= new VMaxPlus(duration,user1);
					
					
					
				}
				
			}
		}
	}
	
	
	public void endRide(Station station, int time) {
		this.duration= time; 
		System.out.println(endTime);
		this.endStation=station;
		for (RidePlan rP : this.velib.getCurrentRidePlan()) {
			if (rP.getBikeType() == this.bike.getType()&&rP.getPath().getEndStation()==this.endStation&&this.user == rP.getUser()) {
				rP.finish();
			}
		}
		
		
		user.setTotalSpentTime(user.getTotalSpentTime()+this.duration);
		if (user.getType()==UserType.Guest ) {
			if (bike.getType()==BicycleType.Mechanical) {
				this.cost=new GuestMech(duration,user);
			}
			else {
				//The bike is electrical
				this.cost=new GuestElec(duration, user);
			}
			
		}
		else if(user.getType()==UserType.Registered) {
			RegisteredUser user1=(RegisteredUser) user;
			if (user1.getCardType()==CardType.VLibre){
				if (endStation.getStationType()==StationType.Standard) {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost=new VLibreStandMech(duration,user1);
					}
					else {
						this.cost=new VLibreStandElec(duration,user1);
					}
				}
				else {
					if (bike.getType()==BicycleType.Mechanical) {
						this.cost= new VLibrePlusMech(duration,user1);
					}
					else {
						this.cost= new VLibrePlusElec(duration,user1);
					}
					
				}
			}
			else {
				if (endStation.getStationType()==StationType.Standard) {
					
					this.cost=new VMaxStand(duration,user1);
					
					
				}
				else {
					
					this.cost= new VMaxPlus(duration,user1);
					
					
					
				}
				
			}
		}
	}
	public static void main(String[] args) {
		
	}
	
	
}
