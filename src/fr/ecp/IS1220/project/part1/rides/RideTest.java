package fr.ecp.IS1220.project.part1.rides;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.bicycles.BicycleFactory;
import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.users.User;
import fr.ecp.IS1220.project.part1.users.UserFactory;
import fr.ecp.IS1220.project.part1.users.UserType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import junit.framework.Assert;

import org.junit.jupiter.api.Test;

class RideTest {
	VelibSystem myVelib = new VelibSystem("myVelib");
	Station stationStand=new Station(new GpsCoordinates(0,0), StationType.Standard, 15, myVelib);
	Station stationPlus=new Station(new GpsCoordinates(1,0), StationType.Plus, 15, myVelib);
	User userGuest=UserFactory.createUser("Wallaert", "Pierre", UserType.Guest, new GpsCoordinates(0, 1) );
	RegisteredUser userLibre=RegisteredUserFactory.createRegisteredUser("Wallaert", "Martin", new GpsCoordinates(2, 0), CardType.VLibre);
	RegisteredUser userMax=RegisteredUserFactory.createRegisteredUser("Wallaert", "Theophile", new GpsCoordinates(2, 0), CardType.VMax);
	Bicycle bikeelec=BicycleFactory.createBicycle(BicycleType.Electrical);
	Bicycle bikemech=BicycleFactory.createBicycle(BicycleType.Mechanical);
	
//Guest user
	@Test
	void GuestMechStandStand() {
		Ride ride=new Ride(userGuest, bikemech, stationStand, stationStand, 30, myVelib);
		
		assertEquals(0.5,(double)ride.cost.cost().get(0));
		
	}
	@Test
	void GuestMechStandPlus() {
		Ride ride=new Ride(userGuest, bikemech, stationStand, stationPlus, 30, myVelib);
		assertEquals(0.5,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestMechPlusPlus() {
		Ride ride=new Ride(userGuest, bikemech, stationPlus, stationPlus, 30, myVelib);
		assertEquals(0.5,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestMechPlusStand() {
		Ride ride=new Ride(userGuest, bikemech, stationPlus, stationStand, 60, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestElecStandStand() {
		Ride ride=new Ride(userGuest, bikeelec, stationStand, stationStand, 30, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestElecStandPlus() {
		Ride ride=new Ride(userGuest, bikeelec, stationStand, stationPlus, 30, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestElecPlusPlus() {
		Ride ride=new Ride(userGuest, bikeelec, stationPlus, stationPlus, 30, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
	}
	@Test
	void GuestElecPlusStand() {
		Ride ride=new Ride(userGuest, bikeelec, stationPlus, stationStand, 60, myVelib);
		assertEquals(2,(double)ride.cost.cost().get(0));
	}
	
	//Vlibre user
	@Test
	void LibreMechPlusCredit() {
		userLibre.setTimeBalance(25);
		Ride ride=new Ride(userLibre, bikemech, stationStand, stationPlus, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(10,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechPlusLimitCredit() {
		userLibre.setTimeBalance(15);
		Ride ride=new Ride(userLibre, bikemech, stationStand, stationPlus, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechPlusInsufficientCredit() {
		userLibre.setTimeBalance(5);
		Ride ride=new Ride(userLibre, bikemech, stationStand, stationPlus, 85, myVelib);
		assertEquals(0.25,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechPlusNoCredit() {
		userLibre.setTimeBalance(0);
		Ride ride=new Ride(userLibre, bikemech, stationStand, stationPlus, 125, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechStandCredit() {
		userLibre.setTimeBalance(25);
		Ride ride=new Ride(userLibre, bikemech, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(5,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechStandLimitCredit() {
		userLibre.setTimeBalance(20);
		Ride ride=new Ride(userLibre, bikemech, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechStandInsufficientCredit() {
		userLibre.setTimeBalance(5);
		Ride ride=new Ride(userLibre, bikemech, stationPlus, stationStand, 95, myVelib);
		assertEquals(0.5,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreMechStandNoCredit() {
		userLibre.setTimeBalance(0);
		Ride ride=new Ride(userLibre, bikemech, stationPlus, stationStand, 120, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecPlusCredit() {
		userLibre.setTimeBalance(25);
		Ride ride=new Ride(userLibre, bikeelec, stationStand, stationPlus, 80, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(10,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecPlusLimitCredit() {
		userLibre.setTimeBalance(15);
		Ride ride=new Ride(userLibre, bikeelec, stationStand, stationPlus, 80, myVelib);
		System.out.println(ride.cost.getClass());
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecPlusInsufficientCredit() {
		userLibre.setTimeBalance(5);
		Ride ride=new Ride(userLibre, bikeelec, stationStand, stationPlus, 85, myVelib);
		assertEquals(1.5,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecPlusNoCredit() {
		userLibre.setTimeBalance(0);
		Ride ride=new Ride(userLibre, bikeelec, stationStand, stationPlus, 125, myVelib);
		assertEquals(3,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecStandCredit() {
		userLibre.setTimeBalance(25);
		Ride ride=new Ride(userLibre, bikeelec, stationPlus, stationStand, 80, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(5,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecStandLimitCredit() {
		userLibre.setTimeBalance(20);
		Ride ride=new Ride(userLibre, bikeelec, stationPlus, stationStand, 80, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecStandInsufficientCredit() {
		userLibre.setTimeBalance(5);
		Ride ride=new Ride(userLibre, bikeelec, stationPlus, stationStand, 95, myVelib);
		assertEquals(2,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void LibreElecStandNoCredit() {
		userLibre.setTimeBalance(0);
		Ride ride=new Ride(userLibre, bikeelec, stationPlus, stationStand, 120, myVelib);
		assertEquals(3,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	
	//Vmax User
	
	@Test
	void MaxMechPlusCredit() {
		userMax.setTimeBalance(25);
		Ride ride=new Ride(userMax, bikemech, stationStand, stationPlus, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(10,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechPlusLimitCredit() {
		userMax.setTimeBalance(15);
		Ride ride=new Ride(userMax, bikemech, stationStand, stationPlus, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechPlusInsufficientCredit() {
		userMax.setTimeBalance(5);
		Ride ride=new Ride(userMax, bikemech, stationStand, stationPlus, 85, myVelib);
		assertEquals(0.25,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechPlusNoCredit() {
		userMax.setTimeBalance(0);
		Ride ride=new Ride(userMax, bikemech, stationStand, stationPlus, 125, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechStandCredit() {
		userMax.setTimeBalance(25);
		Ride ride=new Ride(userMax, bikemech, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(5,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechStandLimitCredit() {
		userMax.setTimeBalance(20);
		Ride ride=new Ride(userMax, bikemech, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechStandInsufficientCredit() {
		userMax.setTimeBalance(5);
		Ride ride=new Ride(userMax, bikemech, stationPlus, stationStand, 95, myVelib);
		assertEquals(0.5,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxMechStandNoCredit() {
		userMax.setTimeBalance(0);
		Ride ride=new Ride(userMax, bikemech, stationPlus, stationStand, 120, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecPlusCredit() {
		userMax.setTimeBalance(25);
		Ride ride=new Ride(userMax, bikeelec, stationStand, stationPlus, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(10,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecPlusLimitCredit() {
		userMax.setTimeBalance(15);
		Ride ride=new Ride(userMax, bikeelec, stationStand, stationPlus, 80, myVelib);
		System.out.println(ride.cost.getClass());
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecPlusInsufficientCredit() {
		userMax.setTimeBalance(5);
		Ride ride=new Ride(userMax, bikeelec, stationStand, stationPlus, 85, myVelib);
		assertEquals(0.25,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecPlusNoCredit() {
		userMax.setTimeBalance(0);
		Ride ride=new Ride(userMax, bikeelec, stationStand, stationPlus, 125, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecStandCredit() {
		userMax.setTimeBalance(25);
		Ride ride=new Ride(userMax, bikeelec, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(5,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecStandLimitCredit() {
		userMax.setTimeBalance(20);
		Ride ride=new Ride(userMax, bikeelec, stationPlus, stationStand, 80, myVelib);
		assertEquals(0,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecStandInsufficientCredit() {
		userMax.setTimeBalance(5);
		Ride ride=new Ride(userMax, bikeelec, stationPlus, stationStand, 95, myVelib);
		assertEquals(0.5,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void MaxElecStandNoCredit() {
		userMax.setTimeBalance(0);
		Ride ride=new Ride(userMax, bikeelec, stationPlus, stationStand, 120, myVelib);
		assertEquals(1,(double)ride.cost.cost().get(0));
		assertEquals(0,(double)ride.cost.cost().get(1));
	}
	@Test
	void testTime() {
		Date date = new Date();   // given date
		Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		calendar.setTime(date);   // assigns calendar to given date 
		int hour=calendar.get(Calendar.MINUTE); // gets hour in 24h format
		calendar.get(Calendar.HOUR);        // gets hour in 12h format
		calendar.get(Calendar.MONTH);       // gets month number, NOTE this is zero based!
		



		
		System.out.println(hour);
	}

}
