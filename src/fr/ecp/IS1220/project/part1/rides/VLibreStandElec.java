package fr.ecp.IS1220.project.part1.rides;

import java.util.ArrayList;

import fr.ecp.IS1220.project.part1.users.CardType;
import fr.ecp.IS1220.project.part1.users.RegisteredUser;
import fr.ecp.IS1220.project.part1.users.RegisteredUserFactory;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

/**
 * Class defining CostStrategy for a Libre user an electric bike that ends on a Standard station
 * @author Pierre
 * @param duration: duration of the ride
 * @param user: user who performed the ride and will be charged 
 */
public class VLibreStandElec implements CostStrategy {
	double duration;
	RegisteredUser user;
	

	
	public VLibreStandElec(double duration, RegisteredUser user) {
		this.user=user;
		this.duration=duration;
	}
	
	@Override
	
	public ArrayList<Double> cost() {
		ArrayList<Double> C=new ArrayList<Double>();
		double cost;
		double TB=user.getTimeBalance();
		if (duration<60) {
			cost=duration/60;
		}
		else if ((60<=duration)&&(duration<=60+TB)) {

			cost=1;

			TB-=(duration-60);
		}
		else {
			
			cost=(duration-60-TB)/60*2+1;
			TB=0;
		}
		C.add(cost);
		C.add(TB);

		return C;
	}



}
