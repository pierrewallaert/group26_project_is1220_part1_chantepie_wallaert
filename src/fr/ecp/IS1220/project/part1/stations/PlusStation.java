package fr.ecp.IS1220.project.part1.stations;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class PlusStation extends Station {


	
	public PlusStation(GpsCoordinates gpsCoord, int numberOfSlots,VelibSystem velib) {
		super(gpsCoord, StationType.Plus, numberOfSlots, velib);
	}
	

}
