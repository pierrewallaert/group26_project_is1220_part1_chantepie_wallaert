package fr.ecp.IS1220.project.part1.stations;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class StationFactory {
	static public Station createStation(GpsCoordinates gpsCoord, StationType stationType, int numberOfSlots, VelibSystem velib) {
		if (stationType == StationType.Standard) {
			return new StandardStation(gpsCoord, numberOfSlots,velib);
		}
		else if (stationType == StationType.Plus) {
			return new PlusStation(gpsCoord, numberOfSlots,velib);
		}
		else {return null;}
	}
}