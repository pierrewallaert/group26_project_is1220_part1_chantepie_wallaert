package fr.ecp.IS1220.project.part1.stations;

/**
 * Implements the status of the stations
 * @author Arnaud
 *
 */

public enum StationStatus {OnService,Offline}
