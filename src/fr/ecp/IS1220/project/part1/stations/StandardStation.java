package fr.ecp.IS1220.project.part1.stations;

import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class StandardStation extends Station {
	
	
	public StandardStation(GpsCoordinates gpsCoord, int numberOfSlots,VelibSystem velib) {
		super(gpsCoord, StationType.Standard, numberOfSlots, velib);
	}
	

}
