package fr.ecp.IS1220.project.part1.stations;

import java.util.ArrayList;
import java.util.Date;

import fr.ecp.IS1220.project.part1.bicycles.BicycleType;
import fr.ecp.IS1220.project.part1.planning.RidePlan;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
import fr.ecp.IS1220.project.part1.util.Moment;

/**
 * Class which creates a Station
 * @author Arnaud
 * @param gpsCoord GPS Coordinate of the station
 * @param stationType Type of the station, either Normal or Plus
 * @param stationStatus Status of the station, either OnService or Offline
 * @param ID The station ID
 * @param numberOfSlots The number of parking slots that the station contains
 * @param slotList The list of slot contains by the station
 * @param terminal The terminal of the station
 */

public class Station {
	GpsCoordinates gpsCoord;
	StationType stationType;
	StationStatus stationStatus;
	static int uniqID = 1;
	int ID;
	int numberOfSlots;
	ArrayList<ParkingSlots> slotList;
	Terminal terminal;
	int rentNumber=0;
	int returnNumber=0;
	VelibSystem velib;
	
	
	
	//Constructors initializing, if necessary, station type to Normal and station status to on service
	public Station(GpsCoordinates gpsCoord, StationType stationType, int numberOfSlots,VelibSystem velib) {
		
		this.ID = uniqID;
		uniqID += 1;  
		this.gpsCoord = gpsCoord;
		this.stationType = stationType;
		this.stationStatus = StationStatus.OnService;
		this.numberOfSlots = numberOfSlots;
		this.terminal = new Terminal(this);
		this.slotList = new ArrayList<ParkingSlots>();
		this.velib=velib;
		
		ParkingSlots.uniqID = 1;
		int N = this.numberOfSlots;
		for (int i = 1; i <= N; i++) {
			new ParkingSlots(this);
		}
		this.velib.getStationsList().add(this);
	}

	
	public void addRent() {
		rentNumber+=1;
	}
	public void addReturn() {
		returnNumber+=1;
	}
	public VelibSystem getVelib() {
		return velib;
	}



	public void setVelib(VelibSystem velib) {
		this.velib = velib;
	}



	public Station(GpsCoordinates gpsCoord, int numberOfSlots, VelibSystem velib) {
		super();
		this.ID = uniqID;
		uniqID += 1;  
		this.gpsCoord = gpsCoord;
		this.stationType = StationType.Standard;
		this.stationStatus = StationStatus.OnService;
		this.numberOfSlots = numberOfSlots;
		this.terminal = new Terminal(this);
		this.slotList = new ArrayList<ParkingSlots>();
		ParkingSlots.uniqID = 1;
		int N = this.numberOfSlots;
		for (int i = 1; i <= N; i++) {
			new ParkingSlots(this);
		}
		this.velib = velib;
		this.velib.getStationsList().add(this);
	}
	
	

	public Station() {
		super();
	}

	//Getters and setters
	public GpsCoordinates getGpsCoord() {
		return gpsCoord;
	}

	public void setGpsCoord(GpsCoordinates gpsCoord) {
		this.gpsCoord = gpsCoord;
	}

	public StationType getStationType() {
		return stationType;
	}

	public void setStationType(StationType stationType) {
		this.stationType = stationType;
	}

	public StationStatus getStationStatus() {
		return stationStatus;
	}

	
	/**
	 * Change the status of the station
	 * @param stationStatus New status of the station
	 * 
	 */
	public void setStationStatus(StationStatus stationStatus) {
		this.stationStatus = stationStatus;
		if (stationStatus==StationStatus.Offline) {
			terminal.status = TerminalStatus.Unavailable;
			for (ParkingSlots PS: slotList) {
				PS.status = ParkingSlotStatus.OutofOrder;
			}
		}
		else if (stationStatus==StationStatus.OnService) {
			terminal.status = TerminalStatus.Available;
			for (ParkingSlots PS: slotList) {
				PS.status = ParkingSlotStatus.Available;
			}
		}
		for (RidePlan rP : this.velib.getCurrentRidePlan()) {
			if (rP.getPath().getEndStation() == this) {
				rP.notifyObs();
			}
		}
	}

	public int getNumberOfSlots() {
		return numberOfSlots;
	}

	public void setNumberOfSlots(int numberOfSlots) {
		this.numberOfSlots = numberOfSlots;
	}

	public ArrayList<ParkingSlots> getSlotList() {
		return slotList;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public int getID() {
		return ID;
	}

	/** Add a slot to a station
	 * @
	 */
	public void addSlot() {
		numberOfSlots += 1;
		int size = slotList.size();
		ParkingSlots.uniqID = size + 1;
		new ParkingSlots(this);
		
	}

	//To String
	@Override
	public String toString() {
		return "\nStation " + stationType + " No " + ID + ", \n\tlocated at " + gpsCoord + ", \n\tactually " + stationStatus + 
				", \n\tcontening " + numberOfSlots + " slots: " + slotList;
	}





	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			Station sobj = (Station) obj;
			if (this.gpsCoord == sobj.gpsCoord && this.ID == sobj.ID) {
				return true;
			}
		}
		return false;
			
	}
	
	public int getNumberOfSlotsWichAre(BicycleType bikeType) {
		int num = 0;
		for (int i=0; i<this.slotList.size();i++) {
			if (this.slotList.get(i).getOccupation() == ParkingSlotOccupation.Occupied && this.slotList.get(i).currentBike.getType() == bikeType) {
				num++;
			}
		}
		return num;
	}
	
	public int getNumberOfSlotsWichAre(ParkingSlotOccupation occup) {
		int num = 0;
		for (int i=0; i<this.slotList.size();i++) {
			if (this.slotList.get(i).getOccupation() == occup) {
				num++;
			}
		}
		return num;
	}
	
	
	public int getNumberOfSlotsWichAre(ParkingSlotStatus status) {
		int num = 0;
		for (int i=0; i<this.slotList.size();i++) {
			if (this.slotList.get(i).getStatus() == status) {
				num++;
			}
		}
		return num;
	}
	
	public double getOccupationRate(Moment ts,Moment te) {
		long sum=0;
		for (ParkingSlots slot :slotList) {
			sum+=slot.getOccupationRate(ts, te);
		}
		double rate=sum/numberOfSlots;
		return(rate);
	}
	
	public double getOccupationRate() {
		double sum=0;
		for (ParkingSlots slot :slotList) {
			sum+=slot.getOccupationRate();
			
		}
		double rate=sum/numberOfSlots;
		return(rate);
	}
	
	public int getActivity() {
		int total=rentNumber+returnNumber;
		
		return(total);
	}
	
	public String showStats() {
		return("Station No" + this.ID + ": \n\t-Number of bike rent: " + this.rentNumber + ";\n\t-Number of bike returned: "
	+ this.returnNumber + ";\n\t-Occupation rate: "+ this.getOccupationRate() + ".");
	}
	
	
}
