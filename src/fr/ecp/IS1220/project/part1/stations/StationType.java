package fr.ecp.IS1220.project.part1.stations;

/**
 * Implements the type of the stations
 * @author Arnaud
 *
 */

public enum StationType {Standard, Plus}
