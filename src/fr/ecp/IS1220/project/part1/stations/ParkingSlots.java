package fr.ecp.IS1220.project.part1.stations;

import java.util.ArrayList;
import java.util.Date;

import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.planning.RidePlan;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.util.Moment;

/**
 * Creates a parking slot
 * @author Arnaud
 * @param stationID ID of the station in which the slot is located
 * @param status Status of the slot (Free, Occupied, OoO : Out of Order)
 */

public class ParkingSlots {
	Station station;
	static int uniqID = 1;
	int ID;
	ParkingSlotStatus status;
	Bicycle currentBike = null;
	ParkingSlotOccupation occup;
	VelibSystem velib;
	
	public Moment lastTime ;
	public ArrayList<Moment[]> occupiedTime;
	public ArrayList<Moment[]> freeTime;
	


	//Constructor only linking to a Station
	/*We assume that the creation of a slot without any
	 *argument for the status implicitely means that this
	 *slots should be free 
	 */
	public ParkingSlots(Station station) {
		super();
		this.station = station;
		this.ID = uniqID;
		uniqID += 1;
		this.status = ParkingSlotStatus.Available;
		this.occup = ParkingSlotOccupation.Free;
		this.lastTime=new Moment();
		this.occupiedTime=new ArrayList<Moment[]>();
		this.freeTime=new ArrayList<Moment[]>();
		this.station.getSlotList().add(this);
		
	
	}
	
	public ParkingSlots(Station station, ParkingSlotOccupation occup, ParkingSlotStatus status) {
		super();
		this.station = station;
		this.ID = uniqID;
		uniqID += 1;
		this.status = status;
		this.occup = occup;
		this.lastTime=new Moment();
		this.occupiedTime=new ArrayList<Moment[]>();
		this.freeTime=new ArrayList<Moment[]>();
	
	}

	//Getters and Setters
	public ParkingSlotStatus getStatus() {
		return status;
	}

	public void setStatus(ParkingSlotStatus status) {
		
		if (status==ParkingSlotStatus.OutofOrder &&this.status==ParkingSlotStatus.Available) {
			if (occup==ParkingSlotOccupation.Occupied) {
				Moment[] time=new Moment[] {lastTime,new Moment()};
				occupiedTime.add(time);
				
			}
			lastTime=new Moment();
		}
		if(status==ParkingSlotStatus.Available &&this.status==ParkingSlotStatus.OutofOrder) {
			Moment[] time=new Moment[] {lastTime,new Moment()};
			occupiedTime.add(time);
			lastTime=new Moment();
			
		}
		this.status = status;
		/*
		 * for (RidePlan rP : this.velib.getCurrentRidePlan()) { if
		 * (rP.getPath().getEndStation() == this.station) { rP.notifyObs(); } }
		 */
	}

	public ParkingSlotOccupation getOccupation() {
		return occup;
	}

	public void setOccupation(ParkingSlotOccupation occup, Bicycle bike) {
		this.occup = occup;
		this.currentBike = bike;
		/*
		 * for (RidePlan rP : this.velib.getCurrentRidePlan()) { if
		 * (rP.getPath().getEndStation() == this.station) { rP.notifyObs(); } }
		 */
	}
	
	public Bicycle getCurrentBike() {
		return currentBike;
	}

	public void setCurrentBike(Bicycle currentBike) {
		this.currentBike = currentBike;
		this.occup = ParkingSlotOccupation.Occupied;
		/*
		 * for (RidePlan rP : this.velib.getCurrentRidePlan()) { if
		 * (rP.getPath().getEndStation() == this.station) { rP.notifyObs(); } }
		 */
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Station getStation() {
		return station;
	}

	public int getID() {
		return ID;
	}

	@Override
	public String toString() {
		return "Parking slot No " + ID + " (" + status + ")\n";
	}
	
	public double getOccupationRate(Moment ts, Moment te) {
		long occupationTime=0;
		Moment i=new Moment(0);
		int k=0;
		//if (occupiedTime.size()==0) {
			//if (this.getOccupation()==ParkingSlotOccupation.Free) {
			//	return(0);
			//}
			//else {
			//	return(1);
			//}
		//}
		
		while((i.before(te))&& k<occupiedTime.size()) {
			if (occupiedTime.get(k)[0].after(ts)) {
				if (occupiedTime.get(k)[1].before(te)) {
					occupationTime+=(occupiedTime.get(k)[1].delay(occupiedTime.get(k)[0]));
					
				}
				else {
					occupationTime+=(te.delay(occupiedTime.get(k)[0])) ;
					
				}
			}
			else if (occupiedTime.get(k)[1].before(te)) {
				occupationTime+=(occupiedTime.get(k)[1].delay(ts)) ;
				
			}
			i=occupiedTime.get(k)[0];
			k+=1;
		}
		if ((occup==ParkingSlotOccupation.Occupied || status==ParkingSlotStatus.OutofOrder)&& (lastTime.before(te))){
			occupationTime+=te.delay(lastTime);
		}
		
		return((double)occupationTime/((double)(te.delay(ts))));
	}
	
	public double getOccupationRate() {
		long occupationTime=0;
		for(Moment[] moment:occupiedTime) {
			occupationTime+=moment[1].delay(moment[0]);
		}
		if ((occup==ParkingSlotOccupation.Occupied || status==ParkingSlotStatus.OutofOrder)){
			occupationTime+=(new Moment()).delay(lastTime);
		}
		return((double)occupationTime/((double)(new Moment().delay(station.getVelib().creation))));
	}

	
	
}
