package fr.ecp.IS1220.project.part1.stations;

public class Terminal {
	Station station;
	int ID;
	static int uniqID;
	TerminalStatus status;
	
	

	//Constructor 
	public Terminal(Station station) {
		super();
		this.station = station;
		this.status = TerminalStatus.Available;
		this.ID = uniqID;
		uniqID += 1;
	}

	//Getters and Setters
	public Station getStation() {
		return station;
	}

	public void setStation(int stationID) {
		this.station = station;
	}

	public TerminalStatus getStatus() {
		return status;
	}

	public void setStatus(TerminalStatus status) {
		this.status = status;
	}
	
	public int getID() {
		return ID;
	}

	@Override
	public String toString() {
		return "Terminal located in station n�" + station + " (" + status + ")";
	}
	

	
	
	
}
