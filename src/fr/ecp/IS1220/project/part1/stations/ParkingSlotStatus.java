package fr.ecp.IS1220.project.part1.stations;

/**
 * possible status for a Parking slot : Free, Occupied, OoO (standing for Out of Order)
 * @author Arnaud
 * 
 */
public enum ParkingSlotStatus {Available, OutofOrder}

