package fr.ecp.IS1220.project.part1.bicycles;
/**
 * Class representing the electrical bicycle, it inheriting frome Bicycle
 * @author Arnaud
 *
 */
public class ElectricalBicycle extends Bicycle {

	public ElectricalBicycle() {
		super(BicycleType.Electrical);
		
	}

}
