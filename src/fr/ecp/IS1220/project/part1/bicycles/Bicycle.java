package fr.ecp.IS1220.project.part1.bicycles;

import java.util.ArrayList;

import fr.ecp.IS1220.project.part1.rides.Ride;

/**
 * Class representing the Bicycles
 * @author Arnaud
 * @param ID the ID of the bike
 * @param type the type of bicycle : Mechanical or Electrical 
 */


public abstract class Bicycle {
	int ID;
	static int uniqID = 1;
	BicycleType type;
	public ArrayList<Ride> rides;
	
	//Constructors
	public Bicycle() {
		super();
		this.ID = uniqID;
		uniqID += 1;
		this.rides=new ArrayList<Ride>();
	}

	

	public Bicycle(BicycleType type) {
		super();
		this.type = type;
		this.ID = uniqID;
		uniqID = 1;
		this.rides=new ArrayList<Ride>();
	}

	//Getters and Setters
	public BicycleType getType() {
		return type;
	}

	public void setType(BicycleType type) {
		this.type = type;
	}

	public int getID() {
		return ID;
	}

	@Override
	public String toString() {
		return type+ " bicycle No" + ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bicycle) {
			Bicycle bobj = (Bicycle) obj;
			if (bobj.ID == this .ID && bobj.type == this.type) {
				return true;
			}
		}
		return false;
	}
	
		
}
