package fr.ecp.IS1220.project.part1.bicycles;

/**
 * Class representing the mechanical bicycle, it inheriting frome Bicycle
 * @author Arnaud
 *
 */

public class MechanicalBicycle extends Bicycle{

	public MechanicalBicycle() {
		super(BicycleType.Mechanical);
		
	}
}
