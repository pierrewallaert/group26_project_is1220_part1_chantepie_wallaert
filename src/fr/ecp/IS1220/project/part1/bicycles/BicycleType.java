package fr.ecp.IS1220.project.part1.bicycles;

/**
 * Enum representing the differents type of bicycles
 * @author Arnaud
 *
 */
public enum BicycleType {Mechanical, Electrical}
