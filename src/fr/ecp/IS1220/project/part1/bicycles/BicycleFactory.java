package fr.ecp.IS1220.project.part1.bicycles;

/**
 * Factory class creating a bicycle based on its type
 * @author Arnaud
 *
 */
public class BicycleFactory {
	
	/**
	 * 
	 * @param type the type of bicycle we want to create
	 * @return a bicycle based on the specified type
	 * 
	 */
	static public Bicycle createBicycle(BicycleType type) {
		if (type == BicycleType.Electrical) {
			return new ElectricalBicycle();
		}
		else if (type == BicycleType.Mechanical) {
			return new MechanicalBicycle();
		}
		else {return null;}
	}
}
