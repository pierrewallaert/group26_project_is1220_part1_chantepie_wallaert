package fr.ecp.IS1220.project.part1.bicycles;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * The client interface in order to create bicycles
 * @author Arnaud
 * 
 */

public class ClientInterfaceBicycle {
	public static void main(String[] args) {
		ArrayList<Bicycle> bicycleList = new ArrayList<Bicycle>();
		Scanner myScanner = new Scanner(System.in);
		System.out.println("What kind of Bicycle de you want to create ?");
		String strType = myScanner.next();
		try{
			BicycleType type = BicycleType.valueOf(strType);
			Bicycle bike = BicycleFactory.createBicycle(type);
			bicycleList.add(bike);
		}
		catch(IllegalArgumentException e) {
			System.err.println("You entered a wrong kind of bike. Only two kind are available for creation : "
					+ "\n\t- Electrical;\n\t- Mechanical.\nPlease enter one of these two kinds");
		}
		finally {
		System.out.println(bicycleList);
		myScanner.close();
		}
	}
}
