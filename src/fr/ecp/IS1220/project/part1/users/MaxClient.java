package fr.ecp.IS1220.project.part1.users;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

/**
 * Class representing the clients holding a VMax Card
 * @author Arnaud
 *
 */
public class MaxClient extends RegisteredUser {

	public MaxClient(String name, String surname, GpsCoordinates position) {
		super(name, surname, position, CardType.VMax);
	}
}
