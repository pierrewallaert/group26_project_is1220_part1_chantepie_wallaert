package fr.ecp.IS1220.project.part1.users;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

/**
 * Class representing the clients holding a VLibre Card
 * @author Arnaud
 *
 */
public class LibreClient extends RegisteredUser{

	public LibreClient(String name, String surname, GpsCoordinates position) {
		super(name, surname, position, CardType.VLibre);
	}
	
	
	
}
