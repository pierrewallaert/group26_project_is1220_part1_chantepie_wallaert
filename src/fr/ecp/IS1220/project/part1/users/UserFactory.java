package fr.ecp.IS1220.project.part1.users;

import java.util.Scanner;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class UserFactory {
	static public User createUser(String name, String surname, UserType type, GpsCoordinates position) {
		if (type == UserType.Guest) {
			return new Guest(name, surname, position);
		}
		if (type == UserType.Registered) {
			Scanner myScanner = new Scanner(System.in);
			System.out.println("What kind of card does your client hold?");
			String strType = myScanner.next();
			try{
				CardType cardType = CardType.valueOf(strType);
				RegisteredUser rU = RegisteredUserFactory.createRegisteredUser(name, surname, position, cardType);
				return rU;
				
			}
			catch(IllegalArgumentException e) {
				System.err.println("You entered a wrong kind of card. Only two kind are available for creation : "
						+ "\n\t- VMax;\n\t- VLibre.\nPlease enter one of these two kinds");
			}
			finally {
				myScanner.close();
			}
			return null;
			
		}
		else {return null;}
	}
}
