package fr.ecp.IS1220.project.part1.users;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

/**
 * Class representing the clients who does not hold a card
 * @author Arnaud
 *
 */
public class Guest extends User {
	public Guest(String name, String surname, GpsCoordinates position) {
		super(name, surname, UserType.Guest, position);
	}


	
	
}
