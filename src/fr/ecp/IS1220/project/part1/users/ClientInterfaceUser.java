package fr.ecp.IS1220.project.part1.users;


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;



public class ClientInterfaceUser {
	
	public User userInterractivecreation() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("What is your user's surname ?");
		String surname = myScanner.nextLine();
		System.out.println("What is your user's name ?");
		String name = myScanner.nextLine();
		System.out.println("What is your user's current latitude ?");
		double latitude = 0;
		try {
			latitude = myScanner.nextDouble();
			System.out.println("What is your user's current longitude ?");
			double longitude;
			try { 
				longitude = myScanner.nextDouble();
				GpsCoordinates position = new GpsCoordinates(latitude, longitude);
				System.out.println("Does your user hold a card ?");
				String strUserType = myScanner.next();
				UserType userType;
				if (strUserType.equalsIgnoreCase("Yes")) {userType = UserType.Registered;}
				else if (strUserType.equalsIgnoreCase("No")) {userType = UserType.Guest;}
				else {System.err.println("You entered a wrong answer. Please answer by Yes or No.");userType =null;System.exit(1);}
					User user = UserFactory.createUser(name, surname, userType, position);
					System.out.println("User created: "+ user);
					myScanner.close();
					return user;
			}
			catch (InputMismatchException e) {
				System.err.println("You did not entered a valid value, please enter a double for each coordinate");

			}
		}
		catch (InputMismatchException e) {
			System.err.println("You did not entered a valid value, please enter a double for each coordinate");

		}
		finally {myScanner.close();}
		
		return null;
		
		
		
		
	}
	public static void main(String[] args) {
		
	}
}
