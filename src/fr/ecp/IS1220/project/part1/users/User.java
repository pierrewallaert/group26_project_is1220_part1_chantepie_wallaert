package fr.ecp.IS1220.project.part1.users;

import java.util.Date;
import java.util.ArrayList;


import fr.ecp.IS1220.project.part1.bicycles.Bicycle;
import fr.ecp.IS1220.project.part1.planning.RidePlan;
import fr.ecp.IS1220.project.part1.planning.VelibSystem;
import fr.ecp.IS1220.project.part1.rides.Ride;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.ParkingSlots;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.util.*;
/**
 * Class representing a ride performed
 * @author Pierre
 * @param name: String representing the name of the user 
 * @param surname: String representing the surname of the user 
 * @param type: SType of the user: Guest, VLibre, Vmax
 * @param ID: unic int ID of a station
 * @param position: Gps Coordinates to locate the user
 * @param rideNumber: number of rides performed by this user
 * @param totalSpentTime: total time on a bike in millis
 * @param totalTimeCredit: total credit earned by this user
 * @param totalCharges: sum of all prices charged to this user
 * @param rides: list of rides performed by this user
 * 
 */

public abstract class User implements Observer{
	String name;
	String surname;
	UserType type;
	int ID;
	static int uniqID = 1;
	GpsCoordinates position;
	int rideNumber;
	double totalSpentTime;
	int totalTimeCredit;
	double totalCharges;
	ArrayList<Ride> rides;
	
	
	

	//Constructors
	public User(String name, String surname, UserType type, GpsCoordinates position) {
		super();
		this.name = name;
		this.surname = surname;
		this.type = type;
		this.position = position;
		this.ID = uniqID;
		uniqID += 1;
		this.rideNumber = 0;
		this.totalSpentTime = 0;
		this.totalTimeCredit = 0;
		this.totalCharges = 0;
		this.rides=new ArrayList<Ride>();
		
		
	}
	
	public User() {
		super();
	}

	//Getters and Setters
	
	public ArrayList<Ride> getRides() {
		return rides;
	}

	public void setRides(ArrayList<Ride> rides) {
		this.rides = rides;
	}
	
	public UserType getType() {
		return type;
	}



	public void setType(UserType type) {
		this.type = type;
	}

	public GpsCoordinates getPosition() {
		return position;
	}

	public void setPosition(GpsCoordinates position) {
		this.position = position;
	}

	public int getID() {
		return ID;
	}
	
		public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public int getRideNumber() {
		return rideNumber;
	}

	public void setRideNumber(int rideNumber) {
		this.rideNumber = rideNumber;
	}

	public double getTotalSpentTime() {
		return totalSpentTime;
	}

	public void setTotalSpentTime(double totalSpentTime) {
		this.totalSpentTime = totalSpentTime;
	}

	public int getTotalTimeCredit() {
		return totalTimeCredit;
	}

	public void setTotalTimeCredit(int totalTimeCredit) {
		this.totalTimeCredit = totalTimeCredit;
	}

	public double getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(double totalCharges) {
		this.totalCharges = totalCharges;
	}

	//To String
	@Override
	public String toString() {
		return "User "+surname+ " " + name + " No" + ID + " (" + type + "), located at " + position;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User uobj = (User) obj;
			if (this.ID == uobj.ID && this.name == uobj.name && this.surname == uobj.surname) {
				return true;
			}
		}
		return false;
			
	}
	/**
	 * method adding a price to the total charge 
	 * @author Pierre
	 * @param: list: Arraylist with the price to paid and then the time credit
	 * 
	 */
	public void charge(ArrayList<Double> list) {
		if (list.size()==2) {
			this.totalCharges+=list.get(0);
			System.out.println("Client has paid:");
			System.out.println(list.get(0));
			

			
		}
	}
	/**
	 * method starting a ride from a slot
	 * @author Pierre
	 * @param: slot: slot where the bike is rent
	 * 
	 */
	public void rent(ParkingSlots slot ) {
		if ((slot.getOccupation()==ParkingSlotOccupation.Occupied)&&(slot.getStatus()==ParkingSlotStatus.Available)) {
			if ((this.rideNumber==0)||(this.rides.get(this.rideNumber-1).getEndStation()!= null)) {
				Bicycle bike=slot.getCurrentBike();
				Station start=slot.getStation();
				Ride ride=new Ride(this, bike, start, start.getVelib());
				this.rides.add(ride);
				this.rideNumber+=1;
				slot.getStation().addRent();
				
				slot.setOccupation(ParkingSlotOccupation.Free, bike);
				Moment[] time=new Moment[] {slot.lastTime,new Moment()};
				slot.occupiedTime.add(time);
				slot.lastTime=new Moment();
			}
			else {
				System.out.println("Already a ride in progress, rental cancelled");
			}
		}
		else {
			System.out.println("Sorry, this bike is not available");
		}
	}
	/**
	 * method ending a rent to the specified slot
	 * @author Pierre
	 * @param: slot: slot where the bike is returned
	 * 
	 */
	public void endRent( ParkingSlots slot) {
		if ((slot.getOccupation()==ParkingSlotOccupation.Free)&&(slot.getStatus()==ParkingSlotStatus.Available)) {
			if (this.rideNumber!=0){
				
				if (this.rides.get(this.rideNumber-1).getEndStation()==null) {
					Ride currentRide=rides.get(rideNumber-1);
					currentRide.endRide(slot.getStation());
					slot.setOccupation(ParkingSlotOccupation.Occupied, this.rides.get(this.rideNumber-1).getBike());
					this.charge(currentRide.cost.cost());
					slot.getStation().addReturn();
					Moment[] time=new Moment[] {slot.lastTime,new Moment()};
					slot.freeTime.add(time);
					slot.lastTime=new Moment();
					}
				else {
					
					System.out.println("No ride in progress, you can't return a bike");
				}
				
			}
			else {
				System.out.println("No ride in progress, you can't return a bike");
			}
		}
		else {
			System.out.println("This slot or this station are unavailable, try another");
		}
		
	}
	/**
	 * method ending a rent to the specified slot
	 * @author Pierre
	 * @param: slot: slot where the bike is returned
	 * @param: time:in millis, the time of the return
	 * 
	 */
	public void endRent( ParkingSlots slot, int time) {
		if ((slot.getOccupation()==ParkingSlotOccupation.Free)&&(slot.getStatus()==ParkingSlotStatus.Available)) {
			if (this.rideNumber!=0){
				
				if (this.rides.get(this.rideNumber-1).getEndStation()==null) {
					Ride currentRide=rides.get(rideNumber-1);
					currentRide.endRide(slot.getStation(), time);
					slot.setOccupation(ParkingSlotOccupation.Occupied, this.rides.get(this.rideNumber-1).getBike());
					this.charge(currentRide.cost.cost());
					slot.getStation().addReturn();
					Moment[] timediff=new Moment[] {slot.lastTime,new Moment(time)};
					slot.freeTime.add(timediff);
					slot.lastTime=new Moment(time);
					}
				else {
					
					System.out.println("No ride in progress, you can't return a bike");
				}
				
			}
			else {
				System.out.println("No ride in progress, you can't return a bike");
			}
		}
		else {
			System.out.println("This slot or this station are unavailable, try another");
		}
		
	}
	@Override
	public void update(RidePlan rP) {
		System.out.println("Your cannot go to this final station anymore, here is your knew end Station" + rP.getPath().getEndStation());
		
	}
	
	public String showStats() {
		return("User " + this.surname + " " + this.name + ": \n\t-total time spent using the system: " + this.totalSpentTime + 
				" min;\n\t-total time credit earned: " + this.totalTimeCredit + " min;\n\t-total charges: "+ this.totalCharges +" �.");
		
	}
	
}
