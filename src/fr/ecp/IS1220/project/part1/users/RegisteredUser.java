package fr.ecp.IS1220.project.part1.users;

import java.util.ArrayList;

import fr.ecp.IS1220.project.part1.rides.Ride;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotOccupation;
import fr.ecp.IS1220.project.part1.stations.ParkingSlotStatus;
import fr.ecp.IS1220.project.part1.stations.ParkingSlots;
import fr.ecp.IS1220.project.part1.stations.Station;
import fr.ecp.IS1220.project.part1.stations.StationType;
import fr.ecp.IS1220.project.part1.util.GpsCoordinates;
/**
 * Class representing the clients holding any type of card
 * @author Arnaud
 * @param cardID the ID of the client's card
 * @param timeBalance the time balance of the client account
 * @param cardType the type of card the client holds (VLibre or VMax)
 */
public abstract class RegisteredUser extends User {
	int cardID;
	CardType cardType;
	double timeBalance;
	static int uniqID;
	
	//Constructors
	public RegisteredUser(String name, String surname, GpsCoordinates position, CardType cardType) {
		super(name, surname, UserType.Registered, position);
		this.timeBalance = 0;
		this.cardID = uniqID;
		this.cardType = cardType;
		uniqID += 1;
	}

	public RegisteredUser() {
		super();
	}
	
	
	//To String
	@Override
	public String toString() {
		return "User n�" + ID + " (" + cardType + "), located at " + position;
	}

	//Getters and Setters
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public double getTimeBalance() {
		return timeBalance;
	}

	public void setTimeBalance(double timeBalance) {
		this.timeBalance = timeBalance;
	}

	public int getCardID() {
		return cardID;
	}
	/**
	 * method adding a price to the total charge and updating Timebalance
	 * @author Pierre
	 * @param: list: Arraylist with the price to paid and then the time credit
	 * 
	 */
	@Override	
	public void charge(ArrayList<Double> list) {
		if (list.size()==2) {
			this.totalCharges+=list.get(0);
			System.out.println("Client has paid:");
			System.out.println(list.get(0));
			this.timeBalance+=list.get(1);
			
			System.out.println("New Time Balance is:");
			System.out.println(list.get(1));
			
		}
	}
	
	@Override
	public void endRent(ParkingSlots slot) {
		if ((slot.getOccupation()==ParkingSlotOccupation.Free)&&(slot.getStatus()==ParkingSlotStatus.Available)) {
			if (this.rideNumber!=0){
				
				if (this.rides.get(this.rideNumber-1).getEndStation()==null) {
					Ride currentRide=rides.get(rideNumber-1);
					currentRide.endRide(slot.getStation());
					slot.setOccupation(ParkingSlotOccupation.Occupied, this.rides.get(this.rideNumber-1).getBike());
					this.charge(currentRide.cost.cost());
					if (currentRide.getEndStation().getStationType()==StationType.Plus) {
						this.totalTimeCredit+=5;
					}
					}
				else {
					
					System.out.println("No ride in progress, you can't return a bike");
				}
				
			}
			else {
				System.out.println("No ride in progress, you can't return a bike");
			}
		}
		else {
			System.out.println("This slot or this station are unavailable, try another");
		}
		
	}	
	
	
	
}
