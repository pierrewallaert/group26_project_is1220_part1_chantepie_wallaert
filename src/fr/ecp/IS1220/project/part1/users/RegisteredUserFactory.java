package fr.ecp.IS1220.project.part1.users;

import fr.ecp.IS1220.project.part1.util.GpsCoordinates;

public class RegisteredUserFactory {
	static public RegisteredUser createRegisteredUser(String name, String surname, GpsCoordinates position, CardType cardType) {
		if (cardType == CardType.VLibre) {
			return new LibreClient(name, surname, position);
		}
		if (cardType == CardType.VMax) {
			return new MaxClient(name, surname, position);
		}
		else {return null;}
	}	
}
